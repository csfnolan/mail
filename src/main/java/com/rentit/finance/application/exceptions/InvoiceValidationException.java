package com.rentit.finance.application.exceptions;

import com.rentit.common.rest.dto.ApiFieldErrorDTO;
import org.springframework.validation.BindingResult;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class InvoiceValidationException extends Exception{
    BindingResult errors;

    public InvoiceValidationException(BindingResult errors) {
        super(String.format("Invoice could not be saved"));
        this.errors = errors;
    }

    public List<ApiFieldErrorDTO> getErrors(){
        return  errors.getFieldErrors()
                .stream()
                .map(fieldError -> ApiFieldErrorDTO.of(
                        fieldError.getField(),
                        fieldError.getCode())
                )
                .collect(toList());
    }
}
