package com.rentit.finance.application.services;


import com.rentit.common.application.exceptions.PlantNotFoundException;
import com.rentit.common.domain.validation.BusinessPeriodValidator;
import com.rentit.common.domain.validation.CustomerValidator;
import com.rentit.finance.application.exceptions.InvoiceNotFoundException;
import com.rentit.finance.application.exceptions.InvoiceValidationException;
import com.rentit.finance.domain.model.Invoice;
import com.rentit.finance.domain.model.InvoiceStatus;
import com.rentit.finance.domain.repository.InvoiceRepository;
import com.rentit.finance.domain.validation.InvoiceValidator;
import com.rentit.sales.application.services.SalesService;
import com.rentit.sales.application.services.exceptions.PurchaseOrderNotFoundException;
import com.rentit.sales.application.services.exceptions.PurchaseOrderValidationException;
import com.rentit.sales.domain.model.PurchaseOrder;
import com.rentit.sales.domain.repository.PurchaseOrderRepository;
import com.rentit.sales.domain.validation.PurchaseOrderValidator;
import jdk.nashorn.internal.runtime.options.Option;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.DataBinder;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class InvoiceService {

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private SalesService salesService;

    @Autowired
    EmailService emailService;

    public List<Invoice> fetchByStatus(InvoiceStatus status) {
        return  invoiceRepository.findByStatus(status);
    }

    public List<Invoice> findAll() {
        return  invoiceRepository.findAll();
    }


    public Invoice createInvoice(LocalDate dueDate, Long poId) throws PurchaseOrderNotFoundException, InvoiceValidationException {
        PurchaseOrder order = salesService.findPurchaseOrder(poId);
        Invoice invoice = Invoice.of(order,dueDate);

        DataBinder binder = new DataBinder(invoice);
        binder.addValidators(new InvoiceValidator());
        binder.validate();

        if (!binder.getBindingResult().hasErrors()) {
            invoiceRepository.save(invoice);
            emailService.sendInvoice(invoice);
        }else{
            throw new InvoiceValidationException(binder.getBindingResult());
        }
        return  invoice;
    }

    public Invoice findInvoice(Long id) throws InvoiceNotFoundException {
        Optional<Invoice> invoiceOption = invoiceRepository.findById(id);
        Invoice invoice;
        if (invoiceOption.isPresent()) {
            invoice = invoiceOption.get();
        } else {
            throw new InvoiceNotFoundException(id);
        }


        return  invoice;
    }

    public Invoice updateInvoice(Long id, InvoiceStatus status) throws InvoiceNotFoundException, PurchaseOrderNotFoundException {
        Invoice invoice = findInvoice(id);
        if(status == InvoiceStatus.OPEN){
            invoice.accept();
            salesService.poInvoiced(invoice.getPurchaseOrder().getId());
        }

        if(status == InvoiceStatus.REJECTED){
            invoice.reject();
        }

        invoiceRepository.save(invoice);

        return invoice;
    }


    public Invoice invoicePayment(Long id, BigDecimal amount) throws InvoiceNotFoundException, PurchaseOrderNotFoundException {
        Invoice invoice = findInvoice(id);
        if(invoice.getPurchaseOrder().getTotal().compareTo(amount)==0){
            invoice.acceptPayment();
            invoiceRepository.save(invoice);

        }

        return invoice;
    }

    public void sendReminder(Long id) throws InvoiceNotFoundException {
        Invoice invoice = findInvoice(id);
        emailService.sendReminder(invoice);
    }
}
