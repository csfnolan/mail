package com.rentit.finance.application.services;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;

@MessagingGateway
@Service
public interface IntegrationService {

        @Gateway(requestChannel = "sendInvoiceChannel")
        public void sendInvoice(MimeMessage msg);

}
