package com.rentit.finance.application.services;

import com.rentit.finance.domain.model.Invoice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;

@Service
public class EmailService {

    @Autowired
    IntegrationService integrationService;

    @Value("${gmail.username}")
    String gmailUsername;

    public void sendInvoice(Invoice invoice) {
        JavaMailSender mailSender = new JavaMailSenderImpl();

        MimeMessage rootMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper;
        try {
            helper = new MimeMessageHelper(rootMessage, true);
            helper.setFrom(gmailUsername);
            helper.setTo(invoice.getPurchaseOrder().getCustomer().getEmail());
            helper.setSubject("Invoice number "+invoice.getId());
            helper.setText("Dear customer,\n\nPlease find attached the invoice for the Purchase order with id number "+invoice.getPurchaseOrder().getId()+".\n\nKindly yours,\n\nTeam 2 RentIt!");

            helper.addAttachment("invoice-"+invoice.getId()+".json", new ByteArrayDataSource(invoiceJson(invoice), "application/json"));
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        integrationService.sendInvoice(rootMessage);
    }

    public void sendReminder(Invoice invoice) {
        JavaMailSender mailSender = new JavaMailSenderImpl();
        MimeMessage rootMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper;
        try {
            helper = new MimeMessageHelper(rootMessage, true);
            helper.setFrom(gmailUsername);
            helper.setTo(invoice.getPurchaseOrder().getCustomer().getEmail());
            helper.setSubject("Reminder: Payment of Invoice number "+invoice.getId());
            helper.setText("Dear customer,\n\nThis is a reminder for the payment of the attached invoice.  Please make sure it is paid by "+invoice.getDueDate()+".\n\nKindly yours,\n\nTeam 2 RentIt!");

            helper.addAttachment("invoice-"+invoice.getId()+".json", new ByteArrayDataSource(invoiceJson(invoice), "application/json"));
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        integrationService.sendInvoice(rootMessage);
    }

    private String invoiceJson(Invoice invoice){
     return
                "{\n" +
                        "  \"id\":"+invoice.getId()+",\n" +
                        "  \"purchaseOrderId\":"+invoice.getPurchaseOrder().getId()+",\n" +
                        "  \"amount\":"+invoice.getPurchaseOrder().getTotal()+",\n" +
                        "  \"dueDate\":\""+invoice.getDueDate()+"\",\n" +
                        "  \"status\":\""+invoice.getStatus()+"\"\n" +
                        "}\n";

    }
}
