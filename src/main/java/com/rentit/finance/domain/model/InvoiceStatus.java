package com.rentit.finance.domain.model;

public enum InvoiceStatus {
    PENDING, OPEN, REJECTED, PAID
}
