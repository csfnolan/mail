package com.rentit.finance.domain.model;

import com.rentit.common.domain.model.BusinessPeriod;
import com.rentit.common.domain.model.Customer;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.sales.domain.model.POStatus;
import com.rentit.sales.domain.model.PurchaseOrder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
public class Invoice {
    @Id
    @GeneratedValue
    Long id;
    LocalDate issueDate;
    LocalDate dueDate;
    InvoiceStatus status;

    @ManyToOne
    PurchaseOrder purchaseOrder;

    public static Invoice of(PurchaseOrder order, LocalDate dueDate) {
        Invoice invoice = new Invoice();
        invoice.dueDate = dueDate;
        invoice.issueDate = LocalDate.now();
        invoice.purchaseOrder = order;
        invoice.status = InvoiceStatus.PENDING;
        return invoice;
    }

    public void accept(){
        status = InvoiceStatus.OPEN;
    }

    public void reject(){
        status = InvoiceStatus.REJECTED;
    }
    public void acceptPayment() {
        status = InvoiceStatus.PAID;
    }
}
