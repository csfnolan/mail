package com.rentit.finance.domain.validation;

import com.rentit.finance.domain.model.Invoice;
import com.rentit.sales.domain.model.POStatus;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class InvoiceValidator implements Validator{

    public InvoiceValidator() {

    }

    @Override
    public boolean supports(Class<?> aClass) {
        return Invoice.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object object, Errors errors) {

        ValidationUtils.rejectIfEmpty(errors,"purchaseOrder","Purchase Order cannot be null");
        ValidationUtils.rejectIfEmpty(errors,"dueDate","Due date cannot be null");

        Invoice invoice = (Invoice) object;

        if(invoice.getDueDate()!=null) {

            if(invoice.getDueDate().getDayOfWeek()== DayOfWeek.SATURDAY || invoice.getDueDate().getDayOfWeek()== DayOfWeek.SUNDAY){
                errors.rejectValue("dueDate", "Invoice due date cannot be on a weekend");
            }

            if (invoice.getDueDate().isBefore(LocalDate.now())) {
                errors.rejectValue("dueDate", "Invoice due date cannot be less than today");
            }
        }

        if(invoice.getPurchaseOrder()!=null){
            if(invoice.getPurchaseOrder().getStatus()!=POStatus.PLANT_RETURNED && invoice.getPurchaseOrder().getStatus()!=POStatus.CANCELLED){
                errors.rejectValue("purchaseOrder", "Purchase Order not closed or equipment not returned");
            }
        }


    }
}
