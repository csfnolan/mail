package com.rentit.finance.rest.dto;

import com.rentit.finance.domain.model.InvoiceStatus;
import com.rentit.sales.rest.dto.PurchaseOrderDTO;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.core.Relation;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Relation(value="invoice", collectionRelation="invoices")
public class InvoiceDto {

    Long _id;
    Long orderId;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate dueDate;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate issueDate;
    String supplier;
    InvoiceStatus status;
    BigDecimal amount;
    String customer;


}
