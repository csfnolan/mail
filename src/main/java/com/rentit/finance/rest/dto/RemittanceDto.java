package com.rentit.finance.rest.dto;

import com.rentit.finance.domain.model.InvoiceStatus;
import com.rentit.sales.rest.dto.PurchaseOrderDTO;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.core.Relation;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class RemittanceDto {
    @Column(precision = 8, scale = 2)
    BigDecimal amount;

}
