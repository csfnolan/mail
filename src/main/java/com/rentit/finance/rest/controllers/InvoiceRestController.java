package com.rentit.finance.rest.controllers;

import com.rentit.common.application.exceptions.PlantNotFoundException;
import com.rentit.common.rest.dto.ApiFieldErrorDTO;
import com.rentit.finance.application.exceptions.InvoiceNotFoundException;
import com.rentit.finance.application.exceptions.InvoiceValidationException;
import com.rentit.finance.application.services.InvoiceService;
import com.rentit.finance.domain.model.InvoiceStatus;
import com.rentit.finance.rest.assemblers.InvoiceAssembler;
import com.rentit.finance.rest.dto.InvoiceDto;
import com.rentit.finance.rest.dto.RemittanceDto;
import com.rentit.sales.application.services.exceptions.PurchaseOrderNotFoundException;
import com.rentit.sales.application.services.exceptions.PurchaseOrderStateChangeException;
import com.rentit.sales.application.services.exceptions.PurchaseOrderValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/finance")
@CrossOrigin
public class InvoiceRestController {

    @Autowired
    InvoiceService invoiceService;

    @Autowired
    InvoiceAssembler invoiceAssembler;

    @GetMapping("/invoices")
    @Secured({"ROLE_ADMIN","ROLE_FINANCE"})
    public Resources<?> findInvoices(@RequestParam(name = "status", required = false) InvoiceStatus status) throws InvoiceNotFoundException, InvoiceValidationException, PurchaseOrderNotFoundException, PurchaseOrderValidationException, PurchaseOrderStateChangeException, PlantNotFoundException {
        if(status !=null){
            return invoiceAssembler.toResources(invoiceService.fetchByStatus(status));
        }else {
            return invoiceAssembler.toResources(invoiceService.findAll());
        }
    }

    @GetMapping("/invoices/{id}")
    @Secured({"ROLE_USER","ROLE_ADMIN","ROLE_FINANCE"})
    public Resource<?> findInvoice(@PathVariable("id") Long id) throws InvoiceNotFoundException, InvoiceValidationException, PurchaseOrderValidationException, PurchaseOrderStateChangeException, PurchaseOrderNotFoundException, PlantNotFoundException {
        return invoiceAssembler.toResource(invoiceService.findInvoice(id));
    }

//    @PostMapping("/invoices")
//    public Resource<?> createInvoice(@RequestBody InvoiceDto partialInvoiceDTO) throws PurchaseOrderNotFoundException, InvoiceNotFoundException, InvoiceValidationException, PurchaseOrderValidationException, PurchaseOrderStateChangeException, PlantNotFoundException {
//        return invoiceAssembler.toResource(invoiceService.createInvoice(partialInvoiceDTO.getDueDate(),partialInvoiceDTO.getOrderId()));
//    }

    @PutMapping("/invoices/{id}")
    @Secured({"ROLE_ADMIN","ROLE_USER"})
    public Resource<?> updateInvoice(@PathVariable("id") Long id, @RequestBody InvoiceDto partialInvoiceDTO) throws PurchaseOrderNotFoundException, InvoiceNotFoundException, InvoiceValidationException, PurchaseOrderValidationException, PurchaseOrderStateChangeException, PlantNotFoundException {
        return invoiceAssembler.toResource(invoiceService.updateInvoice(id,partialInvoiceDTO.getStatus()));
    }

    @PostMapping("/invoices/{id}/remittances")
    @Secured({"ROLE_ADMIN","ROLE_USER"})
    public Resource<?> invoicePayment(@PathVariable("id") Long id, @RequestBody RemittanceDto remittanceDto) throws PurchaseOrderNotFoundException, InvoiceNotFoundException, InvoiceValidationException, PurchaseOrderValidationException, PurchaseOrderStateChangeException, PlantNotFoundException {
        return invoiceAssembler.toResource(invoiceService.invoicePayment(id,remittanceDto.getAmount()));
    }

    @PostMapping("/invoices/{id}/reminders")
    @Secured({"ROLE_ADMIN","ROLE_FINANCE"})
    public ResponseEntity<?> sendReminder(@PathVariable("id") Long id) throws PurchaseOrderNotFoundException, InvoiceNotFoundException, InvoiceValidationException, PurchaseOrderValidationException, PurchaseOrderStateChangeException, PlantNotFoundException {
        invoiceService.sendReminder(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ExceptionHandler(InvoiceNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handInvoiceNotFoundException(InvoiceNotFoundException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler(InvoiceValidationException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public List<ApiFieldErrorDTO> handInvoiceValidationException(InvoiceValidationException ex) {
        return ex.getErrors();
    }

    @ExceptionHandler(PurchaseOrderNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handlePurchaseOrderNotFoundException(PurchaseOrderNotFoundException ex) {
        return ex.getMessage();
    }
}
