package com.rentit.finance.rest.assemblers;

import com.rentit.common.application.exceptions.PlantNotFoundException;
import com.rentit.common.rest.ExtendedLink;
import com.rentit.common.rest.dto.BusinessPeriodDTO;
import com.rentit.common.rest.dto.CustomerDTO;
import com.rentit.finance.application.exceptions.InvoiceNotFoundException;
import com.rentit.finance.application.exceptions.InvoiceValidationException;
import com.rentit.finance.domain.model.Invoice;
import com.rentit.finance.rest.controllers.InvoiceRestController;
import com.rentit.finance.rest.dto.InvoiceDto;
import com.rentit.inventory.rest.assemblers.PlantInventoryEntryAssembler;
import com.rentit.sales.application.services.exceptions.PurchaseOrderNotFoundException;
import com.rentit.sales.application.services.exceptions.PurchaseOrderStateChangeException;
import com.rentit.sales.application.services.exceptions.PurchaseOrderValidationException;
import com.rentit.sales.domain.model.PurchaseOrder;
import com.rentit.sales.rest.assemblers.PurchaseOrderAssembler;
import com.rentit.sales.rest.controllers.SalesRestController;
import com.rentit.sales.rest.dto.PurchaseOrderDTO;
import org.eclipse.jetty.http.HttpMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

@Service
public class InvoiceAssembler {

    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;

    @Autowired
    Environment env;

    public Resource<InvoiceDto> toResource(Invoice invoice) throws PurchaseOrderValidationException, PurchaseOrderNotFoundException, PlantNotFoundException, PurchaseOrderStateChangeException, InvoiceNotFoundException, InvoiceValidationException {
        InvoiceDto dto = new InvoiceDto();
        dto.set_id(invoice.getId());
        dto.setDueDate(invoice.getDueDate());
        dto.setIssueDate(invoice.getIssueDate());
        dto.setSupplier(env.getProperty("supplier.name"));
        dto.setStatus(invoice.getStatus());
        dto.setOrderId(invoice.getPurchaseOrder().getId());
        dto.setAmount(invoice.getPurchaseOrder().getTotal());
        dto.setCustomer(invoice.getPurchaseOrder().getCustomer().getEmail());

        return new Resource<>(
                dto,
                linksFor(invoice)
                );
    }

    public Resources<Resource<InvoiceDto>> toResources(List<Invoice> invoices) throws PurchaseOrderNotFoundException, InvoiceNotFoundException, InvoiceValidationException, PurchaseOrderValidationException, PurchaseOrderStateChangeException, PlantNotFoundException {
        return new Resources<>(
                invoices.stream().map(o -> {
                    try {
                        return toResource(o);
                    } catch (PurchaseOrderValidationException | InvoiceNotFoundException | InvoiceValidationException | PlantNotFoundException | PurchaseOrderStateChangeException | PurchaseOrderNotFoundException  e) {
                        return null;
                    }
                }).collect(Collectors.toList()),
                linkTo(methodOn(InvoiceRestController.class).findInvoices(null)).withSelfRel()
        );
    }

    private List<Link> linksFor(Invoice invoice) throws PurchaseOrderNotFoundException, InvoiceNotFoundException, InvoiceValidationException, PurchaseOrderValidationException, PurchaseOrderStateChangeException, PlantNotFoundException {
        switch (invoice.getStatus()) {
            case OPEN:
                return Arrays.asList(
                        linkTo(methodOn(InvoiceRestController.class).findInvoice(invoice.getId()))
                                .withSelfRel(),
                        new ExtendedLink(linkTo(methodOn(InvoiceRestController.class).sendReminder(invoice.getId())).toString(), "sendReminder", HttpMethod.POST.asString()),
                        new ExtendedLink(linkTo(methodOn(InvoiceRestController.class).invoicePayment(invoice.getId(),null)).toString(), "remittance", HttpMethod.POST.asString())

                );
            case REJECTED:
                return Arrays.asList(
                        linkTo(methodOn(InvoiceRestController.class).findInvoice(invoice.getId()))
                                .withSelfRel()
                );
            case PAID:
                return Arrays.asList(
                        linkTo(methodOn(InvoiceRestController.class).findInvoice(invoice.getId()))
                                .withSelfRel()
                );
            case PENDING:
                return Arrays.asList(
                        linkTo(methodOn(InvoiceRestController.class).findInvoice(invoice.getId()))
                                .withSelfRel(),
                        new ExtendedLink(linkTo(methodOn(InvoiceRestController.class).updateInvoice(invoice.getId(),null)).toString(), "updateInvoice", HttpMethod.PUT.asString())
                );
        }
        return Collections.emptyList();
    }
}