package com.rentit.common.application.exceptions;


public class NoPlantsAvailableException extends Exception{
    public NoPlantsAvailableException(String name) {
        super(String.format("No plants available with name, %s, for the specified period!", name));
    }
}
