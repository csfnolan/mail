package com.rentit.common.domain.validation;

import com.rentit.common.domain.model.BusinessPeriod;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class BusinessPeriodValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return BusinessPeriod.class.equals(aClass);
    }

    @Override
    public void validate(Object object, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors,"startDate","StartDate cannot be null");
        ValidationUtils.rejectIfEmpty(errors,"endDate","EndDate cannot be null");


        BusinessPeriod bp = (BusinessPeriod) object;

        if(bp.getStartDate()!=null && bp.getEndDate()!=null) {

            if(bp.getStartDate().getDayOfWeek()== DayOfWeek.SATURDAY || bp.getStartDate().getDayOfWeek()== DayOfWeek.SUNDAY){
                errors.rejectValue("startDate", "Purchase Order rental period start date cannot be on a weekend");
            }

            if(bp.getEndDate().getDayOfWeek()== DayOfWeek.SATURDAY || bp.getEndDate().getDayOfWeek()== DayOfWeek.SUNDAY){
                errors.rejectValue("endDate", "Purchase Order rental period end date cannot be on a weekend");
            }

            if (bp.getStartDate().isBefore(LocalDate.now())) {
                errors.rejectValue("startDate", "Purchase Order rental period start date cannot be less than today");
            }
            if (bp.getStartDate().isAfter(bp.getEndDate())) {
                errors.rejectValue("startDate", "Purchase Order rental period start date cannot be greater than the end date");
            }
        }

    }
}