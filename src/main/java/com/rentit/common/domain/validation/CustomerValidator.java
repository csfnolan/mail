package com.rentit.common.domain.validation;

import com.rentit.common.domain.model.BusinessPeriod;
import com.rentit.common.domain.model.Customer;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class CustomerValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Customer.class.equals(aClass);
    }

    @Override
    public void validate(Object object, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "name", "Customer name cannot be null");
        ValidationUtils.rejectIfEmpty(errors, "contactPerson", "Customer contact person cannot be null");
        ValidationUtils.rejectIfEmpty(errors, "email", "Customer email cannot be null");
        ValidationUtils.rejectIfEmpty(errors, "siteAddress", "Site address cannot be null");


        Customer customer = (Customer) object;
    }
}