package com.rentit.common.domain.model;

public enum UserRoles {
    SALES, DISPATCHER, ADMIN, USER, FINANCE
}
