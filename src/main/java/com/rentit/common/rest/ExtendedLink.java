package com.rentit.common.rest;

import org.eclipse.jetty.http.HttpMethod;
import org.springframework.hateoas.Link;

public class ExtendedLink extends Link {
    private String method;
    public ExtendedLink(String href, String rel, String method){
        super(href, rel);
        this.method = method;
    }
    public String getMethod(){
        return method;
    }
}
