package com.rentit.sales.domain.validation;

import com.rentit.common.domain.model.BusinessPeriod;
import com.rentit.common.domain.model.Customer;
import com.rentit.inventory.domain.model.PlantReservation;
import com.rentit.sales.domain.model.POExtension;
import com.rentit.sales.domain.model.POStatus;
import com.rentit.sales.domain.model.PurchaseOrder;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.time.DayOfWeek;

public class PurchaseOrderValidator implements Validator{
    private final Validator rentalPeriodValidator;
    private final Validator customerValidator;

    public PurchaseOrderValidator(Validator rentalPeriodValidator, Validator customerValidator) {
        if (rentalPeriodValidator == null || customerValidator== null) {
            throw new IllegalArgumentException("The supplied [Validator] is " +
                    "required and must not be null.");
        }
        if (!rentalPeriodValidator.supports(BusinessPeriod.class)) {
            throw new IllegalArgumentException("The supplied [Validator] must support the validation of [Address] instances.");
        }

        if (!customerValidator.supports(Customer.class)) {
            throw new IllegalArgumentException("The supplied [Validator] must support the validation of [Address] instances.");
        }

        this.rentalPeriodValidator = rentalPeriodValidator;
        this.customerValidator = customerValidator;
    }


    @Override
    public boolean supports(Class<?> aClass) {
        return PurchaseOrder.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object object, Errors errors) {
        PurchaseOrder po = (PurchaseOrder) object;

        if (po.getStatus().equals(POStatus.OPEN) || po.getStatus().equals(POStatus.CANCELLED)) {
            ValidationUtils.rejectIfEmpty(errors,"total","Total cannot be null");
            if(po.getTotal()!= null && po.getTotal().signum()!=1){
                errors.rejectValue("total", "Purchase Order total must have a positive value");
            }
            if (po.getReservations().size() == 0)
                errors.rejectValue("reservations", "Purchase Order missing reservations");

            if(po.getReservations().size()>0){
                PlantReservation reservation = po.getReservations().get(0);
                if(!reservation.getSchedule().equals(po.getRentalPeriod())){
                    errors.rejectValue("reservations.schedule", "Purchase Order Reservation Schedule inconsistent with the Purchase Order Rental Period");
                }

                if(reservation.getPlant()==null){
                    errors.rejectValue("reservations.plant", "Purchase Order Reservation plant cannot be null");
                }
            }
        }

        if(po.getExtensions().size()>0) {
            if (po.pendingExtension() != null) {
                POExtension extension = po.pendingExtension();
                if (extension.getEndDate().isBefore(po.getRentalPeriod().getEndDate()) || extension.getEndDate().isEqual(po.getRentalPeriod().getEndDate())) {
                    errors.rejectValue("extensions", "Extension end date cannot be before the current end date");
                }

                if(extension.getEndDate().getDayOfWeek()== DayOfWeek.SATURDAY || extension.getEndDate().getDayOfWeek()== DayOfWeek.SUNDAY){
                    errors.rejectValue("extensions", "Extension end date cannot be on a weekend");
                }
            }
        }

        errors.pushNestedPath("rentalPeriod");
        ValidationUtils.invokeValidator(this.rentalPeriodValidator, po.getRentalPeriod(), errors);

        errors.popNestedPath();

        errors.pushNestedPath("customer");
        ValidationUtils.invokeValidator(this.customerValidator, po.getCustomer(), errors);
        errors.popNestedPath();
    }
}
