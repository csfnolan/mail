package com.rentit.sales.domain.repository;

import com.rentit.sales.domain.model.POStatus;
import com.rentit.sales.domain.model.PurchaseOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, Long> {

    List<PurchaseOrder> findByStatus(POStatus status);

    @Query(value="select * from PURCHASE_ORDER po join PURCHASE_ORDER_RESERVATIONS por on por.purchase_order_id=po.id where por.reservations_id=?1", nativeQuery=true)
    PurchaseOrder getByReservations(Long id);
    List<PurchaseOrder> findAll();
}
