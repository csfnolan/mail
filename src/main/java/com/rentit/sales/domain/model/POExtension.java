package com.rentit.sales.domain.model;

import com.rentit.inventory.domain.model.PlantInventoryEntry;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Embeddable
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor(staticName = "of")
public class POExtension {
    LocalDate endDate;
    @Enumerated(EnumType.STRING)
    POExtensionStatus status;

    public void setStatus(POExtensionStatus status){
        this.status = status;
    }

}
