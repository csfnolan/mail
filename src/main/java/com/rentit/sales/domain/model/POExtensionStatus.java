package com.rentit.sales.domain.model;

public enum POExtensionStatus {
    PENDING, ACCEPTED, REJECTED
}
