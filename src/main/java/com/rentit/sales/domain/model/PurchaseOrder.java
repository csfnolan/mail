package com.rentit.sales.domain.model;

import com.rentit.common.domain.model.BusinessPeriod;
import com.rentit.common.domain.model.Customer;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.model.PlantReservation;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
public class PurchaseOrder {
    @Id @GeneratedValue
    Long id;

    @OneToMany
    List<PlantReservation> reservations = new ArrayList<>();
    @ManyToOne
    PlantInventoryEntry plant;

    LocalDate issueDate;

    @Column(precision = 8, scale = 2)
    BigDecimal total;

    @Enumerated(EnumType.STRING)
    POStatus status;

    @Embedded
    BusinessPeriod rentalPeriod;

    @Embedded
    Customer customer;

    String notes;

    @ElementCollection
    List<POExtension> extensions = new ArrayList<>();

    public static PurchaseOrder of(PlantInventoryEntry plant, BusinessPeriod rentalPeriod, Customer customer) {
        PurchaseOrder po = new PurchaseOrder();
        po.plant = plant;
        po.issueDate = LocalDate.now();
        po.rentalPeriod = rentalPeriod;
        po.customer = customer;
        po.status = POStatus.PENDING;
        return po;
    }

    public POExtension requestExtension(LocalDate endDate) {
        POExtension extension = pendingExtension();
        if(extension==null){
            extension = POExtension.of(endDate,POExtensionStatus.PENDING);
            extensions.add(extension);
        }
        return extension;
    }

    public POExtension pendingExtension() {
        POExtension result = null;
        if (extensions.size() > 0) {
            for (POExtension ext:extensions) {
                if (ext.getStatus() == POExtensionStatus.PENDING)
                    return ext;
            }

        }
        return result;
    }

    public void acceptExtension(PlantReservation reservation) {
        POExtension extension = pendingExtension();
        if(extension!=null){
            extension.setStatus(POExtensionStatus.ACCEPTED);
            reservations.add(reservation);
            rentalPeriod = BusinessPeriod.of(rentalPeriod.getStartDate(), reservation.getSchedule().getEndDate());
            total = updateTotal(plant.getPrice());
        }
    }

    public void rejectExtension() {
        POExtension extension = pendingExtension();
        if(extension!=null){
            extension.setStatus(POExtensionStatus.REJECTED);
        }
    }

    private BigDecimal updateTotal (BigDecimal amount) {
        return amount.multiply(new BigDecimal(rentalPeriod.countWorkingDays())) ;
    }

    public void reject() {
        status = POStatus.REJECTED;
    }
    public void acceptPurhaseOrder (PlantReservation reservation) {
        reservations.add(reservation);
        status = POStatus.OPEN;
        total = updateTotal(plant.getPrice());
    }

    public void addNotes(String notes) {
        this.notes = notes;
    }

    public void cancel() {
        status = POStatus.CANCELLED;
        reservations.clear();
    }

    public void dispatch() {
        status = POStatus.PLANT_DISPATCHED;
    }

    public void acceptDelivery() {
        status = POStatus.PLANT_DELIVERED;
    }

    public void rejectDelivery() {
        status = POStatus.PLANT_REJECTED_BY_CUSTOMER;
        reservations.clear();
    }


    public void returnPlant() {
        status = POStatus.PLANT_RETURNED;
        reservations.clear();
    }

    public void invoice() {
        status = POStatus.INVOICED;
    }


}
