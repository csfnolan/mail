package com.rentit.sales.application.services.exceptions;

public class PurchaseOrderNotFoundException extends Exception{
    public PurchaseOrderNotFoundException(Long id) {
        super(String.format("Purchase Order not found! (Purchase Order id: %d)", id));
    }
}
