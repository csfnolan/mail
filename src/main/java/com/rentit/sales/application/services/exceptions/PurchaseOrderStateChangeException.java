package com.rentit.sales.application.services.exceptions;

public class PurchaseOrderStateChangeException extends Exception{
    public PurchaseOrderStateChangeException(String message) {
        super(message);
    }
}
