package com.rentit.sales.application.services;

import com.rentit.common.application.exceptions.PlantNotFoundException;
import com.rentit.common.domain.model.BusinessPeriod;
import com.rentit.common.domain.model.Customer;
import com.rentit.common.domain.validation.BusinessPeriodValidator;
import com.rentit.common.domain.validation.CustomerValidator;
import com.rentit.common.rest.dto.CustomerDTO;
import com.rentit.finance.application.exceptions.InvoiceNotFoundException;
import com.rentit.finance.application.exceptions.InvoiceValidationException;
import com.rentit.finance.application.services.InvoiceService;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.model.PlantInventoryItem;
import com.rentit.inventory.domain.model.PlantReservation;
import com.rentit.inventory.domain.repository.InventoryRepository;
import com.rentit.inventory.domain.repository.PlantInventoryEntryRepository;
import com.rentit.inventory.domain.repository.PlantInventoryItemRepository;
import com.rentit.inventory.domain.repository.PlantReservationRepository;
import com.rentit.inventory.rest.assemblers.DispatchAssembler;
import com.rentit.inventory.rest.dto.DispatchDto;
import com.rentit.sales.application.services.exceptions.PurchaseOrderStateChangeException;
import com.rentit.sales.application.services.exceptions.PurchaseOrderNotFoundException;
import com.rentit.sales.application.services.exceptions.PurchaseOrderValidationException;
import com.rentit.sales.domain.model.POExtension;
import com.rentit.sales.domain.model.POStatus;
import com.rentit.sales.domain.model.PurchaseOrder;
import com.rentit.sales.domain.repository.PurchaseOrderRepository;
import com.rentit.sales.domain.validation.PurchaseOrderValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.stereotype.Service;
import org.springframework.validation.DataBinder;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SalesService {

    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;
    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepository;
    @Autowired
    PlantReservationRepository plantReservationRepository;
    @Autowired
    InventoryRepository inventoryRepository;

    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    InvoiceService invoiceService;

    @Autowired
    DispatchAssembler dispatchAssembler;

    public PurchaseOrder findPurchaseOrder(Long id) throws PurchaseOrderNotFoundException {
        Optional<PurchaseOrder> poOption = purchaseOrderRepository.findById(id);
        if (poOption.isPresent()) {
            return poOption.get();
        } else {
            throw new PurchaseOrderNotFoundException(id);
        }
    }

    public PurchaseOrder createPurchaseOrder(Long plantId, LocalDate startDate, LocalDate endDate, CustomerDTO customer) throws PurchaseOrderValidationException, PlantNotFoundException {
        Optional<PlantInventoryEntry> plantOption = plantInventoryEntryRepository.findById(plantId);
        PlantInventoryEntry plant;
        if (plantOption.isPresent()) {
            plant = plantOption.get();
        } else {
            throw new PlantNotFoundException(plantId);
        }
        List<PlantInventoryItem> plantInventoryItems = inventoryRepository.findAvailableItems(plant, startDate, endDate);
        PurchaseOrder po = PurchaseOrder.of(
                plant,
                BusinessPeriod.of(startDate, endDate),
                Customer.of(customer.getName(), customer.getContactPerson(), customer.getEmail(), customer.getSiteAddress()));

        DataBinder binder = new DataBinder(po);
        binder.addValidators(new PurchaseOrderValidator(new BusinessPeriodValidator(), new CustomerValidator()));
        binder.validate();

        if (!binder.getBindingResult().hasErrors()) {
            if (plantInventoryItems.isEmpty()) {
                List<PlantInventoryEntry> result = inventoryRepository.findAvailablePlants(plant.getPlantInventoryCategory().getId(), startDate, endDate);
                if (result.isEmpty()) {
                    po.addNotes("No plant available to service your PO");
                } else {
                    po.addNotes(String.format("Plant unavailable at the moment. We however have %s available", result.get(0).getDescription()));
                }
                po.reject();
            } else {
                PlantReservation plantReservation = PlantReservation.of(plantInventoryItemRepository.getOne(plantInventoryItems.get(0).getId()),
                        po.getRentalPeriod());

                plantReservationRepository.save(plantReservation);
                po.acceptPurhaseOrder(plantReservation);
            }
            purchaseOrderRepository.save(po);
        } else {
            throw new PurchaseOrderValidationException(binder.getBindingResult());
        }

        return po;
    }

    public PurchaseOrder requestPurchaseOrderExtension(Long id, LocalDate endDate) throws PurchaseOrderNotFoundException, PurchaseOrderValidationException, PurchaseOrderStateChangeException {
        Optional<PurchaseOrder> poOption = purchaseOrderRepository.findById(id);

        PurchaseOrder po;
        if (poOption.isPresent()) {
            po = poOption.get();
        } else {
            throw new PurchaseOrderNotFoundException(id);
        }
        if (po.getStatus() != POStatus.PLANT_DELIVERED) {
            throw new PurchaseOrderStateChangeException("Purchase order cannot be extended at this stage");
        }else{
            POExtension extension = po.requestExtension(endDate);
            if(extension==null){
                throw new PurchaseOrderStateChangeException("Cannot create an extension, you have another pending extension request");
            }
            DataBinder binder = new DataBinder(po);
            binder.addValidators(new PurchaseOrderValidator(new BusinessPeriodValidator(), new CustomerValidator()));
            binder.validate();

            if (!binder.getBindingResult().hasErrors()) {
                List<PlantInventoryItem> plantInventoryItems = inventoryRepository.findAvailableItems(po.getPlant(), po.getRentalPeriod().getEndDate(), endDate);
                if (plantInventoryItems.isEmpty()) {
                    List<PlantInventoryEntry> result = inventoryRepository.findAvailablePlants(po.getPlant().getPlantInventoryCategory().getId(), po.getRentalPeriod().getEndDate(), endDate);
                    if (result.isEmpty()) {
                        po.rejectExtension();
                        po.addNotes("No plant available for the extension period");
                    } else {
                        PlantInventoryEntry replacement = null;
                        for (PlantInventoryEntry plant:result) {
                            if(plant.getPrice().divide(po.getPlant().getPrice(),2,RoundingMode.HALF_UP).multiply(new BigDecimal(100)).compareTo(new BigDecimal(130))>0){
                                replacement = plant;
                                break;
                            }
                        }
                        if(replacement!=null){
                            List<PlantInventoryItem> items = inventoryRepository.findAvailableItems(replacement,po.getRentalPeriod().getEndDate(),endDate);
                            PlantReservation plantReservation = PlantReservation.of(items.get(0), BusinessPeriod.of(po.getRentalPeriod().getEndDate(), endDate));
                            plantReservationRepository.save(plantReservation);
                            po.acceptExtension(plantReservation);
                            po.addNotes("Plant unavailable but a replacement has been allocated");
                        }else{
                            po.rejectExtension();
                            po.addNotes("No plant available for the extension period");
                        }
                    }
                } else {
                    PlantInventoryItem item = po.getReservations().get(po.getReservations().size() - 1).getPlant();
                    PlantReservation plantReservation = PlantReservation.of(item, BusinessPeriod.of(po.getRentalPeriod().getEndDate(), endDate));
                    plantReservationRepository.save(plantReservation);
                    po.acceptExtension(plantReservation);

                }
                purchaseOrderRepository.save(po);
            } else {
                throw new PurchaseOrderValidationException(binder.getBindingResult());
            }
        }
        return po;
    }

    public PurchaseOrder cancelPurchaseOrder(Long id) throws PurchaseOrderStateChangeException, PurchaseOrderNotFoundException {
        PurchaseOrder po = findPurchaseOrder(id);

        if (po.getStatus()!= POStatus.OPEN) {
            throw new PurchaseOrderStateChangeException("Purchase Order cannot be cancelled at this stage");
        }
        else {
            List<Long> reservations = new ArrayList<>();
            for (PlantReservation r:po.getReservations()
                 ) {
                reservations.add(r.getId());
            }
            po.cancel();
            plantReservationRepository.deleteAll(plantReservationRepository.findAllById(reservations));
            purchaseOrderRepository.save(po);
        }
        return po;
    }


    public PurchaseOrder dispatchPlant(Long id) throws PurchaseOrderNotFoundException, PurchaseOrderStateChangeException {
        PurchaseOrder po = findPurchaseOrder(id);
        if (po.getStatus() != POStatus.OPEN) {
            throw new PurchaseOrderStateChangeException("Plant cannot be dispatched at this stage");
        } else {
            po.dispatch();
            return purchaseOrderRepository.save(po);
        }
    }

    public PurchaseOrder acceptDelivery(Long id) throws PurchaseOrderNotFoundException, PurchaseOrderStateChangeException {
        PurchaseOrder po = findPurchaseOrder(id);
        if (po.getStatus() != POStatus.PLANT_DISPATCHED) {
            throw new PurchaseOrderStateChangeException("Cannot accept delivery at this stage");
        } else {
            po.acceptDelivery();
            return purchaseOrderRepository.save(po);
        }
    }

    public PurchaseOrder rejectDelivery(Long id) throws PurchaseOrderNotFoundException, PurchaseOrderStateChangeException {
        PurchaseOrder po = findPurchaseOrder(id);
        if (po.getStatus() != POStatus.PLANT_DISPATCHED) {
            throw new PurchaseOrderStateChangeException("Cannot reject plant at this stage");
        } else {
            List<PlantReservation> reservations = po.getReservations();
            po.rejectDelivery();
            purchaseOrderRepository.save(po);
            plantReservationRepository.deleteAll(reservations);
            return po;
        }
    }

    public PurchaseOrder returnPlant(Long id) throws PurchaseOrderStateChangeException, PurchaseOrderNotFoundException, InvoiceValidationException {
        PurchaseOrder po = findPurchaseOrder(id);
        if (po.getStatus() != POStatus.PLANT_DELIVERED) {
            throw new PurchaseOrderStateChangeException("Plant cannot be returned at this stage");
        } else {
            List<PlantReservation> reservations = po.getReservations();
            po.returnPlant();
            purchaseOrderRepository.save(po);
            plantReservationRepository.deleteAll(reservations);

            invoiceService.createInvoice(LocalDate.now().plusDays(15),po.getId());
            purchaseOrderRepository.save(po);
            return po;
        }
    }

    public PurchaseOrder invoice(Long id) throws PurchaseOrderStateChangeException, PurchaseOrderNotFoundException {
        PurchaseOrder po = findPurchaseOrder(id);
        if (po.getStatus() != POStatus.PLANT_RETURNED) {
            throw new PurchaseOrderStateChangeException("Plant cannot be returned at this stage");
        } else {
            po.invoice();
            return purchaseOrderRepository.save(po);
        }
    }

    public List<PurchaseOrder> getAllPurchaseOrders(POStatus status) {

        return purchaseOrderRepository.findByStatus(status);
    }

    public PurchaseOrder acceptPOExtension(Long plantId, Long poId) throws PurchaseOrderNotFoundException, PlantNotFoundException, PurchaseOrderStateChangeException {
        PurchaseOrder po = findPurchaseOrder(poId);
        Optional<PlantInventoryEntry> plantOption = plantInventoryEntryRepository.findById(plantId);
        PlantInventoryEntry plant;
        if (plantOption.isPresent()) {
            plant = plantOption.get();
        } else {
            throw new PlantNotFoundException(plantId);
        }

        POExtension extension = po.pendingExtension();
        if(extension!=null) {
            PlantReservation reservation = null;
            for (PlantReservation r:po.getReservations()) {
                if(r.getPlant().equals(plant)){
                    reservation = r;
                }
            }
            if(reservation!=null){
                po.acceptExtension(reservation);
            }else{
                throw new PurchaseOrderStateChangeException("Cannot accept extension! No pending extension found");
            }

        }else{
            throw new PurchaseOrderStateChangeException("Cannot accept extension! No pending extension found");

        }

        return po;
    }

    public PurchaseOrder rejectPOExtension(Long id) throws PurchaseOrderNotFoundException, PurchaseOrderStateChangeException {
        PurchaseOrder po = findPurchaseOrder(id);

        POExtension extension = po.pendingExtension();
        if(extension!=null) {
            po.rejectExtension();
            po.addNotes("");
            purchaseOrderRepository.save(po);
        }else{
            throw new PurchaseOrderStateChangeException("Cannot reject extension! No pending extension found");
        }
        return po;
    }

    public void poInvoiced(Long id) throws PurchaseOrderNotFoundException {
        PurchaseOrder po = findPurchaseOrder(id);
        po.invoice();
        purchaseOrderRepository.save(po);
    }

    public List<Resource<DispatchDto>> findDeliveries(LocalDate date) throws PlantNotFoundException, InvoiceValidationException, PurchaseOrderStateChangeException, PurchaseOrderValidationException, PurchaseOrderNotFoundException, InvoiceNotFoundException {
        List<Resource<DispatchDto>> result = new ArrayList<>();
        List<PlantReservation> reservations = plantReservationRepository.findByScheduleStartDate(date);
        for (PlantReservation reservation:reservations) {
            PurchaseOrder order = getByReservationId(reservation.getId());
            if(order.getStatus()==POStatus.OPEN)
                result.add(dispatchAssembler.toResource(reservation.getPlant(),order));
        }

        return result;
    }

    private PurchaseOrder getByReservationId(Long id) {
        PurchaseOrder result = purchaseOrderRepository.getByReservations(id);

        return result;
    }
}