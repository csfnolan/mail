package com.rentit.sales.rest.controllers;

import com.rentit.common.application.exceptions.NoPlantsAvailableException;
import com.rentit.common.application.exceptions.PlantNotFoundException;
import com.rentit.common.rest.dto.ApiFieldErrorDTO;
import com.rentit.finance.application.exceptions.InvoiceNotFoundException;
import com.rentit.finance.application.exceptions.InvoiceValidationException;
import com.rentit.inventory.rest.assemblers.DispatchAssembler;
import com.rentit.inventory.rest.assemblers.PlantReservationAssembler;
import com.rentit.inventory.rest.dto.DispatchDto;
import com.rentit.inventory.rest.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.application.services.InventoryService;
import com.rentit.inventory.rest.assemblers.PlantInventoryEntryAssembler;
import com.rentit.inventory.rest.dto.PlantReservationDTO;
import com.rentit.sales.application.services.exceptions.PurchaseOrderStateChangeException;
import com.rentit.sales.application.services.exceptions.PurchaseOrderNotFoundException;
import com.rentit.sales.application.services.exceptions.PurchaseOrderValidationException;
import com.rentit.sales.domain.model.POStatus;
import com.rentit.sales.rest.dto.POExtensionDTO;
import com.rentit.sales.rest.dto.PurchaseOrderDTO;
import com.rentit.sales.rest.assemblers.POExtensionAssembler;
import com.rentit.sales.rest.assemblers.PurchaseOrderAssembler;
import com.rentit.sales.application.services.SalesService;
import com.rentit.sales.domain.model.PurchaseOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@RestController
@RequestMapping("/api/sales")
@CrossOrigin
public class SalesRestController {
    @Autowired
    InventoryService inventoryService;
    @Autowired
    SalesService salesService;
    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;
    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;
    @Autowired
    POExtensionAssembler poExtensionAssembler;

    @Autowired
    PlantReservationAssembler plantReservationAssembler;

    @Autowired
    DispatchAssembler dispatchAssembler;


    @GetMapping("/plants")
    public Resources<?> findAvailablePlants(
            @RequestParam(name = "name") String plantName,
            @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) throws NoPlantsAvailableException {
        return plantInventoryEntryAssembler.toResources(inventoryService.findAvailable(plantName, startDate, endDate));
    }

    @PostMapping("/orders")
    public ResponseEntity<?> createPurchaseOrder(@RequestBody PurchaseOrderDTO partialPODTO) throws PlantNotFoundException, PurchaseOrderValidationException, PurchaseOrderNotFoundException, PurchaseOrderStateChangeException, InvoiceNotFoundException, InvoiceValidationException {

        PlantInventoryEntryDTO plant = partialPODTO.getPlant().getContent();
        PurchaseOrder newlyCreatedPO = salesService.createPurchaseOrder(plant.get_id(), partialPODTO.getRentalPeriod().getStartDate(), partialPODTO.getRentalPeriod().getEndDate(), partialPODTO.getCustomer());

        Resource<PurchaseOrderDTO> resource = purchaseOrderAssembler.toResource(newlyCreatedPO);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Location", resource.getRequiredLink("self").getHref());

        return new ResponseEntity<>(
                resource,
                headers, HttpStatus.CREATED);
    }

    @GetMapping("/orders/transfers")
    @Secured({"ROLE_ADMIN","ROLE_DISPATCHER","ROLE_SALES"})
    public List<Resource<DispatchDto>> fetchDeliveries(@RequestParam(name = "date", required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) throws PurchaseOrderValidationException, PurchaseOrderNotFoundException, PlantNotFoundException, PurchaseOrderStateChangeException, InvoiceNotFoundException, InvoiceValidationException {
        return salesService.findDeliveries(date);
    }

    @GetMapping("/orders/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Secured({"ROLE_ADMIN","ROLE_USER","ROLE_SALES"})
    public Resource<PurchaseOrderDTO> fetchPurchaseOrder(@PathVariable("id") Long id) throws PurchaseOrderValidationException, PurchaseOrderNotFoundException, PlantNotFoundException, PurchaseOrderStateChangeException, InvoiceNotFoundException, InvoiceValidationException {
        return purchaseOrderAssembler.toResource(salesService.findPurchaseOrder(id));
    }

    @DeleteMapping("/orders/{id}")
    @Secured({"ROLE_ADMIN","ROLE_USER","ROLE_SALES"})
    public Resource<PurchaseOrderDTO> cancelPO(@PathVariable("id") Long id) throws PurchaseOrderStateChangeException, PurchaseOrderNotFoundException, PurchaseOrderValidationException, PlantNotFoundException, InvoiceNotFoundException, InvoiceValidationException {
        return purchaseOrderAssembler.toResource(salesService.cancelPurchaseOrder(id));
    }

    @GetMapping("/orders")
    @Secured({"ROLE_ADMIN","ROLE_SALES"})
    public Resources<?> getAllPurchaseOrders(@RequestParam(name = "status",required = true) POStatus status) throws PlantNotFoundException, PurchaseOrderValidationException, PurchaseOrderNotFoundException, PurchaseOrderStateChangeException {
        return purchaseOrderAssembler.toResources(salesService.getAllPurchaseOrders(status));
    }

    @GetMapping("/orders/{id}/extensions")
    @Secured({"ROLE_ADMIN","ROLE_USER","ROLE_SALES"})
    public Resources<Resource<POExtensionDTO>> retrievePurchaseOrderExtensions(@PathVariable("id") Long id) throws PlantNotFoundException, PurchaseOrderValidationException, PurchaseOrderNotFoundException, PurchaseOrderStateChangeException, InvoiceNotFoundException, InvoiceValidationException {
        PurchaseOrder order = salesService.findPurchaseOrder(id);
        return poExtensionAssembler.toResources(order.getExtensions(),order);
    }

    @GetMapping("/orders/{id}/reservations")
    @Secured({"ROLE_ADMIN","ROLE_USER","ROLE_SALES"})
    public Resources<Resource<PlantReservationDTO>> retrievePOReservations(@PathVariable("id") Long id) throws PurchaseOrderNotFoundException {
        PurchaseOrder order = salesService.findPurchaseOrder(id);
        return plantReservationAssembler.toResources(order.getReservations(),order);
    }

//    @PatchMapping("/orders/{id}/extensions")
//    public Resource<?> acceptPOExtension(@RequestBody PlantInventoryEntryDTO partialPlantDTO, @PathVariable("id") Long id) throws PlantNotFoundException, PurchaseOrderValidationException, PurchaseOrderNotFoundException, PurchaseOrderStateChangeException, InvoiceNotFoundException, InvoiceValidationException {
//        return purchaseOrderAssembler.toResource(salesService.acceptPOExtension(partialPlantDTO.get_id(), id));
//    }

//    @DeleteMapping("/orders/{id}/extensions")
//    public Resource<?> rejectPOExtension(@PathVariable("id") Long id) throws PlantNotFoundException, PurchaseOrderValidationException, PurchaseOrderNotFoundException, PurchaseOrderStateChangeException, InvoiceNotFoundException, InvoiceValidationException {
//        return purchaseOrderAssembler.toResource(salesService.rejectPOExtension(id));
//    }

    @PostMapping("/orders/{id}/extensions")
    @Secured({"ROLE_ADMIN","ROLE_USER","ROLE_SALES"})
    public Resource<?> requestPurchaseOrderExtension(@RequestBody POExtensionDTO extension, @PathVariable("id") Long id) throws PlantNotFoundException, PurchaseOrderNotFoundException, PurchaseOrderValidationException, PurchaseOrderStateChangeException, InvoiceNotFoundException, InvoiceValidationException {
        return purchaseOrderAssembler.toResource(salesService.requestPurchaseOrderExtension(id, extension.getEndDate()));
    }

    @PostMapping("/orders/{id}/dispatch")
    @Secured({"ROLE_ADMIN","ROLE_DISPATCHER"})
    public Resource<?> handleDispatch(@PathVariable("id") Long id) throws PlantNotFoundException, PurchaseOrderNotFoundException, PurchaseOrderValidationException, PurchaseOrderStateChangeException, InvoiceNotFoundException, InvoiceValidationException {
        return purchaseOrderAssembler.toResource(salesService.dispatchPlant(id));
    }

    @DeleteMapping("/orders/{id}/dispatch")
    @Secured({"ROLE_ADMIN","ROLE_DISPATCHER"})
    public Resource<?> handlePlantReturn(@PathVariable("id") Long id) throws PlantNotFoundException, PurchaseOrderNotFoundException, PurchaseOrderValidationException, PurchaseOrderStateChangeException, InvoiceNotFoundException, InvoiceValidationException {
        return purchaseOrderAssembler.toResource(salesService.returnPlant(id));
    }

    @PostMapping("/orders/{id}/delivery")
    @Secured({"ROLE_ADMIN","ROLE_DISPATCHER"})
    public Resource<?> handleAcceptDelivery(@PathVariable("id") Long id) throws PlantNotFoundException, PurchaseOrderNotFoundException, PurchaseOrderValidationException, PurchaseOrderStateChangeException, InvoiceNotFoundException, InvoiceValidationException {
        return purchaseOrderAssembler.toResource(salesService.acceptDelivery(id));
    }

    @DeleteMapping("/orders/{id}/delivery")
    @Secured({"ROLE_ADMIN","ROLE_DISPATCHER"})
    public Resource<?> handleRejectDelivery(@PathVariable("id") Long id) throws PlantNotFoundException, PurchaseOrderNotFoundException, PurchaseOrderValidationException, PurchaseOrderStateChangeException, InvoiceNotFoundException, InvoiceValidationException {
        return purchaseOrderAssembler.toResource(salesService.rejectDelivery(id));
    }

    @ExceptionHandler(PlantNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handlePlantNotFoundException(PlantNotFoundException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler(NoPlantsAvailableException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleNoPlantsAvailableException(NoPlantsAvailableException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler(PurchaseOrderValidationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public List<ApiFieldErrorDTO> handCustomValidationException(PurchaseOrderValidationException ex) {
        return ex.getErrors();
    }

    @ExceptionHandler(PurchaseOrderStateChangeException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handPOCancellationException(PurchaseOrderStateChangeException ex) {
        return ex.getMessage();
    }


    @ExceptionHandler(PurchaseOrderNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handlePurchaseOrderNotFoundException(PurchaseOrderNotFoundException ex) {
        return ex.getMessage();
    }
}
