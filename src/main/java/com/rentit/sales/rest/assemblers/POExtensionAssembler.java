package com.rentit.sales.rest.assemblers;

import com.rentit.common.application.exceptions.PlantNotFoundException;
import com.rentit.common.rest.ExtendedLink;
import com.rentit.finance.application.exceptions.InvoiceNotFoundException;
import com.rentit.finance.application.exceptions.InvoiceValidationException;
import com.rentit.sales.application.services.exceptions.PurchaseOrderStateChangeException;
import com.rentit.sales.application.services.exceptions.PurchaseOrderNotFoundException;
import com.rentit.sales.application.services.exceptions.PurchaseOrderValidationException;
import com.rentit.sales.rest.dto.POExtensionDTO;
import com.rentit.sales.domain.model.POExtension;
import com.rentit.sales.domain.model.PurchaseOrder;
import com.rentit.sales.rest.controllers.SalesRestController;
import org.eclipse.jetty.http.HttpMethod;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.afford;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Service
public class POExtensionAssembler {
    public Resource<POExtensionDTO> toResource(POExtension extension, PurchaseOrder order) throws PlantNotFoundException, PurchaseOrderValidationException, PurchaseOrderNotFoundException, PurchaseOrderStateChangeException {
        POExtensionDTO dto = new POExtensionDTO();
        dto.setEndDate(extension.getEndDate());
        dto.setStatus(extension.getStatus());

        return new Resource<>(
                dto
        );
    }

    public Resources<Resource<POExtensionDTO>> toResources(List<POExtension> extensions, PurchaseOrder order) throws PlantNotFoundException, PurchaseOrderValidationException, PurchaseOrderNotFoundException, PurchaseOrderStateChangeException, InvoiceNotFoundException, InvoiceValidationException {
        return new Resources<>(
                extensions.stream().map(o -> {
                    try {
                        return toResource(o, order);
                    } catch (PlantNotFoundException e) {
                        return null;
                    } catch (PurchaseOrderValidationException e) {
                        return null;
                    } catch (PurchaseOrderNotFoundException e) {
                        return null;
                    } catch (PurchaseOrderStateChangeException e) {
                        return null;
                    }
                }).collect(Collectors.toList()),
                linkTo(methodOn(SalesRestController.class).retrievePurchaseOrderExtensions(order.getId())).withSelfRel(),
                new ExtendedLink(linkTo(methodOn(SalesRestController.class).requestPurchaseOrderExtension(null, order.getId())).toString(), "requestPOExtension", HttpMethod.POST.asString())
        );
    }
}
