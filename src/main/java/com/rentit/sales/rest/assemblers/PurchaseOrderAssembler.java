package com.rentit.sales.rest.assemblers;

import com.rentit.common.application.exceptions.PlantNotFoundException;
import com.rentit.common.rest.ExtendedLink;
import com.rentit.common.rest.dto.BusinessPeriodDTO;
import com.rentit.common.rest.dto.CustomerDTO;
import com.rentit.finance.application.exceptions.InvoiceNotFoundException;
import com.rentit.finance.application.exceptions.InvoiceValidationException;
import com.rentit.inventory.rest.assemblers.PlantInventoryEntryAssembler;
import com.rentit.sales.application.services.exceptions.PurchaseOrderStateChangeException;
import com.rentit.sales.application.services.exceptions.PurchaseOrderNotFoundException;
import com.rentit.sales.application.services.exceptions.PurchaseOrderValidationException;
import com.rentit.sales.rest.dto.PurchaseOrderDTO;
import com.rentit.sales.domain.model.PurchaseOrder;
import com.rentit.sales.rest.controllers.SalesRestController;
import org.eclipse.jetty.http.HttpMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Service
public class PurchaseOrderAssembler {

    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    public Resource<PurchaseOrderDTO> toResource(PurchaseOrder po) throws PurchaseOrderValidationException, PurchaseOrderNotFoundException, PlantNotFoundException, PurchaseOrderStateChangeException, InvoiceNotFoundException, InvoiceValidationException {
        PurchaseOrderDTO dto = new PurchaseOrderDTO();
        dto.set_id(po.getId());
        if (po.getPlant() != null)
            dto.setPlant(plantInventoryEntryAssembler.toResource(po.getPlant()));
        dto.setRentalPeriod(BusinessPeriodDTO.of(po.getRentalPeriod().getStartDate(), po.getRentalPeriod().getEndDate()));
        dto.setTotal(po.getTotal());
        dto.setStatus(po.getStatus());
        dto.setCustomer(CustomerDTO.of(po.getCustomer().getName(), po.getCustomer().getContactPerson(), po.getCustomer().getEmail(), po.getCustomer().getSiteAddress()));
        dto.setNotes(po.getNotes());
        return new Resource<>(
                dto,
                linksFor(po)
                );
    }

    public Resources<Resource<PurchaseOrderDTO>> toResources(List<PurchaseOrder> orders) throws PlantNotFoundException, PurchaseOrderValidationException, PurchaseOrderNotFoundException, PurchaseOrderStateChangeException {
        return new Resources<>(
                orders.stream().map(o -> {
                    try {
                        return toResource(o);
                    } catch (PlantNotFoundException | PurchaseOrderValidationException | PurchaseOrderNotFoundException | PurchaseOrderStateChangeException | InvoiceNotFoundException | InvoiceValidationException e) {
                        return null;
                    }
                }).collect(Collectors.toList()),
                linkTo(methodOn(SalesRestController.class).getAllPurchaseOrders(null)).withSelfRel()
        );
    }

    private List<Link> linksFor(PurchaseOrder order) throws PurchaseOrderValidationException, PurchaseOrderNotFoundException, PlantNotFoundException, PurchaseOrderStateChangeException, InvoiceNotFoundException, InvoiceValidationException {
        switch (order.getStatus()) {
            case OPEN:
                return Arrays.asList(
                        linkTo(methodOn(SalesRestController.class).fetchPurchaseOrder(order.getId()))
                                .withSelfRel(),
                        new ExtendedLink(linkTo(methodOn(SalesRestController.class).handleDispatch(order.getId())).toString(), "dispatch", HttpMethod.POST.asString()),
                        new ExtendedLink(linkTo(methodOn(SalesRestController.class).cancelPO(order.getId())).toString(), "cancelPO", HttpMethod.DELETE.asString()),
                        new ExtendedLink(linkTo(methodOn(SalesRestController.class).retrievePOReservations(order.getId())).toString(), "reservations", HttpMethod.GET.asString())
                );
            case PLANT_DISPATCHED:
                return Arrays.asList(
                        linkTo(methodOn(SalesRestController.class).fetchPurchaseOrder(order.getId()))
                                .withSelfRel(),
                        new ExtendedLink(linkTo(methodOn(SalesRestController.class).retrievePOReservations(order.getId())).toString(), "reservations", HttpMethod.GET.asString()),
                        new ExtendedLink(linkTo(methodOn(SalesRestController.class).handleAcceptDelivery(order.getId())).toString(), "acceptDelivery", HttpMethod.POST.asString()),
                        new ExtendedLink(linkTo(methodOn(SalesRestController.class).handleRejectDelivery(order.getId())).toString(), "rejectDelivery", HttpMethod.DELETE.asString())
                );
            case PLANT_REJECTED_BY_CUSTOMER:
                return Arrays.asList(
                        linkTo(methodOn(SalesRestController.class).fetchPurchaseOrder(order.getId()))
                                .withSelfRel()
                );
            case PLANT_DELIVERED:
                return Arrays.asList(
                        linkTo(methodOn(SalesRestController.class).fetchPurchaseOrder(order.getId()))
                                .withSelfRel(),
                        new ExtendedLink(linkTo(methodOn(SalesRestController.class).retrievePurchaseOrderExtensions(order.getId())).toString(), "extensions", HttpMethod.GET.asString()),
                        new ExtendedLink(linkTo(methodOn(SalesRestController.class).retrievePOReservations(order.getId())).toString(), "reservations", HttpMethod.GET.asString()),
                        new ExtendedLink(linkTo(methodOn(SalesRestController.class).requestPurchaseOrderExtension(null, order.getId())).toString(), "requestPOExtension", HttpMethod.POST.asString()),
                        new ExtendedLink(linkTo(methodOn(SalesRestController.class).handlePlantReturn(order.getId())).toString(), "returnPlant", HttpMethod.DELETE.asString())
                );
//            case PENDING_EXTENSION:
//                return Arrays.asList(
//                        linkTo(methodOn(SalesRestController.class).fetchPurchaseOrder(order.getId()))
//                                .withSelfRel(),
//                        new ExtendedLink(linkTo(methodOn(SalesRestController.class).acceptPOExtension(null, order.getId())).toString(), "acceptPOExtension", "PATCH"),
//                        new ExtendedLink(linkTo(methodOn(SalesRestController.class).rejectPOExtension(order.getId())).toString(), "rejectPOExtension", "DELETE")
//                );
            case PLANT_RETURNED:
                return Arrays.asList(
                        linkTo(methodOn(SalesRestController.class).fetchPurchaseOrder(order.getId()))
                                .withSelfRel()
                );
            case INVOICED:
                return Arrays.asList(
                        linkTo(methodOn(SalesRestController.class).fetchPurchaseOrder(order.getId()))
                                .withSelfRel()
                );
            case REJECTED:
                return Arrays.asList(
                        linkTo(methodOn(SalesRestController.class).fetchPurchaseOrder(order.getId()))
                                .withSelfRel()
                );
            case CANCELLED:
                return Arrays.asList(
                        linkTo(methodOn(SalesRestController.class).fetchPurchaseOrder(order.getId()))
                                .withSelfRel()
                );
        }
        return Collections.emptyList();
    }
}