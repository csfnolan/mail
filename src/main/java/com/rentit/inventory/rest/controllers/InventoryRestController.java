package com.rentit.inventory.rest.controllers;

import com.rentit.common.application.exceptions.PlantNotFoundException;
import com.rentit.finance.application.exceptions.InvoiceNotFoundException;
import com.rentit.finance.application.exceptions.InvoiceValidationException;
import com.rentit.inventory.application.services.InventoryService;
import com.rentit.inventory.rest.assemblers.PlantInventoryEntryAssembler;
import com.rentit.sales.application.services.exceptions.PurchaseOrderStateChangeException;
import com.rentit.sales.application.services.exceptions.PurchaseOrderNotFoundException;
import com.rentit.sales.application.services.exceptions.PurchaseOrderValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/inventory")
public class InventoryRestController {

    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    @Autowired
    InventoryService inventoryService;

    @GetMapping("/plants/{id}")
    public Resource<?> getPlantInventoryEntry(@PathVariable("id") Long id) throws PlantNotFoundException, PurchaseOrderValidationException, PurchaseOrderNotFoundException, PurchaseOrderStateChangeException, InvoiceNotFoundException, InvoiceValidationException {
        return plantInventoryEntryAssembler.toResource(inventoryService.getPlant(id));
    }

    @ExceptionHandler(PlantNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handlePlantNotFoundException(PlantNotFoundException ex) {
        return ex.getMessage();
    }
}
