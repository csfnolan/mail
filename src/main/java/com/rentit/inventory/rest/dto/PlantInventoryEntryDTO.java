package com.rentit.inventory.rest.dto;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

import javax.persistence.Column;
import java.math.BigDecimal;

@Data
@Relation(value="plant", collectionRelation="plants")
public class PlantInventoryEntryDTO extends ResourceSupport {
    Long _id;
    String name;
    String description;
    @Column(precision = 8, scale = 2)
    BigDecimal price;
    String category;
    String supplier;
}
