package com.rentit.inventory.rest.dto;

import lombok.Data;
import org.springframework.hateoas.ResourceSupport;

@Data
public class PlantInventoryItemDTO extends ResourceSupport {
    Long _id;
    String serialNumber;
    PlantInventoryEntryDTO plant;
}
