package com.rentit.inventory.rest.dto;

import com.rentit.common.domain.model.Customer;
import com.rentit.inventory.rest.dto.PlantInventoryItemDTO;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class DispatchDto {
    Long orderId;
    String contactPerson;
    String email;
    String deliveryAddress;
    PlantInventoryItemDTO plant;

}
