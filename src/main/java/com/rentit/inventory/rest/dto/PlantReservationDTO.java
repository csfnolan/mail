package com.rentit.inventory.rest.dto;

import com.rentit.common.rest.dto.BusinessPeriodDTO;
import lombok.Data;

@Data
public class PlantReservationDTO {
    Long _id;
    PlantInventoryItemDTO plantInfo;
    BusinessPeriodDTO schedule;
}
