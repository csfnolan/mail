package com.rentit.inventory.rest.assemblers;

import com.rentit.common.application.exceptions.PlantNotFoundException;
import com.rentit.common.rest.ExtendedLink;
import com.rentit.finance.application.exceptions.InvoiceNotFoundException;
import com.rentit.finance.application.exceptions.InvoiceValidationException;
import com.rentit.inventory.rest.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.rest.controllers.InventoryRestController;
import com.rentit.sales.application.services.exceptions.PurchaseOrderStateChangeException;
import com.rentit.sales.application.services.exceptions.PurchaseOrderNotFoundException;
import com.rentit.sales.application.services.exceptions.PurchaseOrderValidationException;
import com.rentit.sales.rest.controllers.SalesRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Service
public class PlantInventoryEntryAssembler {

    @Autowired
    Environment env;
    public Resource<PlantInventoryEntryDTO> toResource(PlantInventoryEntry plant) throws PlantNotFoundException, PurchaseOrderValidationException, PurchaseOrderNotFoundException, PurchaseOrderStateChangeException, InvoiceNotFoundException, InvoiceValidationException {
        PlantInventoryEntryDTO dto = new PlantInventoryEntryDTO();
        dto.set_id(plant.getId());
        dto.setName(plant.getName());
        dto.setDescription(plant.getDescription());
        dto.setPrice(plant.getPrice());
        dto.setCategory(plant.getPlantInventoryCategory().getName());
        dto.setSupplier(env.getProperty("supplier.name"));

        return new Resource<>(
                dto,
                linkTo(methodOn(InventoryRestController.class).getPlantInventoryEntry(plant.getId()))
                        .withSelfRel(),
                new ExtendedLink(linkTo(methodOn(SalesRestController.class).createPurchaseOrder(null)).toString(), "createPO", "POST")
        );
    }

    public Resources<Resource<PlantInventoryEntryDTO>> toResources(List<PlantInventoryEntry> plants) {
        return new Resources<>(
                plants.stream().map(plant -> {
                    try {
                        return toResource(plant);
                    } catch (PlantNotFoundException | PurchaseOrderValidationException | PurchaseOrderNotFoundException | PurchaseOrderStateChangeException | InvoiceNotFoundException | InvoiceValidationException e) {
                        return null;
                    }
                }).collect(Collectors.toList())
        );
    }
}