package com.rentit.inventory.rest.assemblers;

import com.rentit.inventory.rest.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.rest.dto.PlantInventoryItemDTO;
import com.rentit.inventory.domain.model.PlantInventoryItem;
import com.rentit.inventory.rest.controllers.PlantInventoryItemRestController;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Service
public class PlantInventoryItemAssembler {

    public Resource<PlantInventoryItemDTO> toResource(PlantInventoryItem plant) {
        PlantInventoryItemDTO dto = new PlantInventoryItemDTO();
        dto.set_id(plant.getId());
        dto.setSerialNumber(plant.getSerialNumber());
        PlantInventoryEntryDTO info = new PlantInventoryEntryDTO();
        info.setCategory(plant.getPlantInfo().getPlantInventoryCategory().getName());
        info.set_id(plant.getPlantInfo().getId());
        info.setDescription(plant.getPlantInfo().getDescription());
        info.setName(plant.getPlantInfo().getName());
        info.setPrice(plant.getPlantInfo().getPrice());
        dto.setPlant(info);
        return new Resource<>(
                dto,
                linkTo(methodOn(PlantInventoryItemRestController.class).findPlant(plant.getId())).withSelfRel()
        );
    }

    public Resources<Resource<PlantInventoryItemDTO>> toResources(List<PlantInventoryItem> items) {
        return new Resources<>(
                items.stream().map(i -> toResource(i)).collect(Collectors.toList())
        );
    }
}