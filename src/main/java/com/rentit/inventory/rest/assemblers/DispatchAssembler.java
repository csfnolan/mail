package com.rentit.inventory.rest.assemblers;

import com.rentit.common.application.exceptions.PlantNotFoundException;
import com.rentit.common.rest.ExtendedLink;
import com.rentit.finance.application.exceptions.InvoiceNotFoundException;
import com.rentit.finance.application.exceptions.InvoiceValidationException;
import com.rentit.finance.rest.controllers.InvoiceRestController;
import com.rentit.inventory.rest.dto.DispatchDto;
import com.rentit.inventory.domain.model.PlantInventoryItem;
import com.rentit.sales.application.services.exceptions.PurchaseOrderNotFoundException;
import com.rentit.sales.application.services.exceptions.PurchaseOrderStateChangeException;
import com.rentit.sales.application.services.exceptions.PurchaseOrderValidationException;
import com.rentit.sales.domain.model.PurchaseOrder;
import com.rentit.sales.rest.assemblers.PurchaseOrderAssembler;
import com.rentit.sales.rest.controllers.SalesRestController;
import org.eclipse.jetty.http.HttpMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Service
public class DispatchAssembler {

    @Autowired
    PlantInventoryItemAssembler plantInventoryItemAssembler;

    @Autowired
    Environment env;

    public Resource<DispatchDto> toResource(PlantInventoryItem plant, PurchaseOrder order) throws PurchaseOrderValidationException, PurchaseOrderNotFoundException, PlantNotFoundException, PurchaseOrderStateChangeException, InvoiceNotFoundException, InvoiceValidationException {
        DispatchDto dto = new DispatchDto();
        dto.setContactPerson(order.getCustomer().getContactPerson());
        dto.setDeliveryAddress(order.getCustomer().getSiteAddress());
        dto.setEmail(order.getCustomer().getEmail());
        dto.setOrderId(order.getId());
        dto.setPlant(plantInventoryItemAssembler.toResource(plant).getContent());

        return new Resource<>(
                dto,
                linksFor(order)
                );
    }

    public Resources<Resource<DispatchDto>> toResources(List<PlantInventoryItem> items, PurchaseOrder order) throws PurchaseOrderNotFoundException, InvoiceNotFoundException, InvoiceValidationException, PurchaseOrderValidationException, PurchaseOrderStateChangeException, PlantNotFoundException {
        return new Resources<>(
                items.stream().map(i -> {
                    try {
                        return toResource(i,order);
                    } catch (PurchaseOrderValidationException | InvoiceNotFoundException | InvoiceValidationException | PlantNotFoundException | PurchaseOrderStateChangeException | PurchaseOrderNotFoundException  e) {
                        return null;
                    }
                }).collect(Collectors.toList()),
                linkTo(methodOn(InvoiceRestController.class).findInvoices(null)).withSelfRel()
        );
    }

    private List<Link> linksFor(PurchaseOrder order) throws PurchaseOrderNotFoundException, InvoiceNotFoundException, InvoiceValidationException, PurchaseOrderValidationException, PurchaseOrderStateChangeException, PlantNotFoundException {
                return Arrays.asList(
                        linkTo(methodOn(SalesRestController.class).fetchDeliveries(null))
                                .withSelfRel(),
                        new ExtendedLink(linkTo(methodOn(SalesRestController.class).fetchPurchaseOrder(order.getId())).toString(), "viewPO", HttpMethod.GET.asString()),
                        new ExtendedLink(linkTo(methodOn(SalesRestController.class).handleDispatch(order.getId())).toString(), "dispatch", HttpMethod.POST.asString())
                );

    }
}