package com.rentit.inventory.rest.assemblers;

import com.rentit.common.application.exceptions.PlantNotFoundException;
import com.rentit.common.rest.ExtendedLink;
import com.rentit.common.rest.dto.BusinessPeriodDTO;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.model.PlantInventoryItem;
import com.rentit.inventory.rest.controllers.InventoryRestController;
import com.rentit.inventory.rest.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.rest.dto.PlantInventoryItemDTO;
import com.rentit.inventory.rest.dto.PlantReservationDTO;
import com.rentit.inventory.domain.model.PlantReservation;
import com.rentit.sales.application.services.exceptions.PurchaseOrderNotFoundException;
import com.rentit.sales.application.services.exceptions.PurchaseOrderStateChangeException;
import com.rentit.sales.application.services.exceptions.PurchaseOrderValidationException;
import com.rentit.sales.domain.model.PurchaseOrder;
import com.rentit.sales.rest.controllers.SalesRestController;
import org.eclipse.jetty.http.HttpMethod;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Service
public class PlantReservationAssembler {

    public Resource<PlantReservationDTO> toResource(PlantReservation reservation) {
        PlantReservationDTO dto = new PlantReservationDTO();
        dto.set_id(reservation.getId());
        dto.setPlantInfo(toInventoryItemDTO(reservation.getPlant()));
        dto.setSchedule(BusinessPeriodDTO.of(reservation.getSchedule().getStartDate(),reservation.getSchedule().getEndDate()));
        return new Resource<>(
                dto
        );
    }

    public Resources<Resource<PlantReservationDTO>> toResources(List<PlantReservation> reservations, PurchaseOrder order) throws PurchaseOrderNotFoundException {
        return new Resources<>(
                reservations.stream().map(reservation -> toResource(reservation)).collect(Collectors.toList())
        ,
        linkTo(methodOn(SalesRestController.class).retrievePOReservations(order.getId())).withSelfRel()
        );
    }

    private PlantInventoryItemDTO toInventoryItemDTO(PlantInventoryItem item){
        PlantInventoryItemDTO dto = new PlantInventoryItemDTO();
        dto.set_id(item.getId());
        dto.setPlant(toInventoryEntryDTO(item.getPlantInfo()));
        dto.setSerialNumber(item.getSerialNumber());
        return dto;
    }

    private PlantInventoryEntryDTO toInventoryEntryDTO(PlantInventoryEntry plant){
        PlantInventoryEntryDTO dto = new PlantInventoryEntryDTO();
        dto.set_id(plant.getId());
        dto.setName(plant.getName());
        dto.setDescription(plant.getDescription());
        dto.setPrice(plant.getPrice());
        dto.setCategory(plant.getPlantInventoryCategory().getName());
        return dto;
    }

}