package com.rentit.inventory.domain.model;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
public class PlantInventoryEntry {
    @Id
    @GeneratedValue
    Long id;

    String name;
    String description;

    @ManyToOne
    PlantInventoryCategory plantInventoryCategory;

    @Column(precision = 8, scale = 2)
    BigDecimal price;
}
