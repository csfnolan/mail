package com.rentit.inventory.domain.model;

import com.rentit.common.domain.model.BusinessPeriod;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class PlantInventoryCategory {
    @Id @GeneratedValue
    Long id;

    String name;
}
