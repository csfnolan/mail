package com.rentit.inventory.domain.model;

import com.rentit.common.domain.model.BusinessPeriod;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class PlantReservation {
    @Id @GeneratedValue
    Long id;

    @ManyToOne
    PlantInventoryItem plant;

    @Embedded
    BusinessPeriod schedule;

    public static PlantReservation of(PlantInventoryItem plant, BusinessPeriod schedule) {
        PlantReservation pr = new PlantReservation();
        pr.plant = plant;
        pr.schedule = schedule;
        return pr;
    }
}
