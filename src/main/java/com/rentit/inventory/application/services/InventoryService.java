package com.rentit.inventory.application.services;

import com.rentit.common.application.exceptions.NoPlantsAvailableException;
import com.rentit.common.application.exceptions.PlantNotFoundException;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.repository.InventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class InventoryService {

    @Autowired
    InventoryRepository inventoryRepository;

    public List<PlantInventoryEntry> findAvailable(String plantName, LocalDate startDate, LocalDate endDate) throws NoPlantsAvailableException {
        List<PlantInventoryEntry> result = inventoryRepository.findAvailablePlants(plantName,startDate, endDate);
        if(result.isEmpty()){
            throw new NoPlantsAvailableException(plantName);
        }
        return result;
    }

    public PlantInventoryEntry getPlant(Long id) throws PlantNotFoundException {
        PlantInventoryEntry plant = inventoryRepository.getOne(id);

        if(plant==null){
            throw new PlantNotFoundException(id);
        }else{
            return plant;
        }
    }
}
