package com.rentit.security.application.service;

import java.io.Serializable;
import java.util.List;

public class JwtAuthenticationResponse implements Serializable {

    private static final long serialVersionUID = 1250166508152483573L;

    private final String token;
    private List<String> roles;

    public JwtAuthenticationResponse(String token, List<String> roles) {
        this.token = token;
        this.roles = roles;
    }

    public String getToken() {
        return this.token;
    }
    public List<String>  getRoles() {
        return this.roles;
    }
}

