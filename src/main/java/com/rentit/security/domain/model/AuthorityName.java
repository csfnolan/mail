package com.rentit.security.domain.model;

public enum AuthorityName {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_SALES,
    ROLE_DISPATCHER,
    ROLE_FINANCE
}
