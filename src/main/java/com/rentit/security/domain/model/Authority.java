package com.rentit.security.domain.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "AUTHORITY")
@Data
public class Authority {

//    @Id
//    @Column(name = "ID")
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "authority_seq")
//    @SequenceGenerator(name = "authority_seq", sequenceName = "authority_seq", allocationSize = 1)
//    private Long id;

    //    @Column(name = "NAME", length = 50)
//    @NotNull
    @Id
    @Enumerated(EnumType.STRING)
    private AuthorityName name;

    @ManyToMany(mappedBy = "authorities", fetch = FetchType.LAZY)
    private List<User> users;

    public Authority(AuthorityName name){
        this.name = name;
    }

    public Authority(){

    }

}
