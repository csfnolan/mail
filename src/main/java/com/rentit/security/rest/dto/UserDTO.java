package com.rentit.security.rest.dto;

import com.rentit.security.domain.model.User;
import lombok.Data;

@Data
public class UserDTO {

    private String username;
    private String firstname;
    private String lastname;
    private String email;

    public static UserDTO of(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.email = user.getEmail();
        userDTO.username = user.getUsername();
        userDTO.firstname = user.getFirstname();
        userDTO.lastname = user.getLastname();
        return userDTO;
    }
}
