package com.rentit.security.rest.controllers;

import com.rentit.security.application.jwt.JwtAuthenticationRequest;
import com.rentit.security.application.jwt.JwtTokenUtil;
import com.rentit.security.application.jwt.JwtUser;
import com.rentit.security.application.service.JwtAuthenticationResponse;
import com.rentit.security.application.service.JwtUserDetailsService;
import com.rentit.security.domain.model.Authority;
import com.rentit.security.domain.model.AuthorityName;
import com.rentit.security.domain.model.User;
import com.rentit.security.rest.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/api/auth")
@CrossOrigin
public class AuthenticationRestController {
    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;

    @Autowired
    @Qualifier("jwtUserDetailsService")
    private UserDetailsService userDetailsService;

    @PostMapping("/login")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest) throws AuthenticationException {
        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        // Reload password post-security so we can generate the token
        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        final String token = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new JwtAuthenticationResponse(token,getRoles(userDetails)));
    }

    private List<String> getRoles(UserDetails userDetails){
        List<String> roles = new ArrayList<>();
        for (GrantedAuthority a:userDetails.getAuthorities()
                ) {
            roles.add(a.getAuthority());
        }

        return  roles;
    }

    @PostMapping("/signup")
    public ResponseEntity<?> createUser(@RequestBody User authenticationRequest) throws AuthenticationException {
        if (jwtUserDetailsService.userExists(authenticationRequest.getUsername())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Username already exists");
        } else {

            authenticationRequest.setEnabled(true);
            authenticationRequest.setPassword(passwordEncoder.encode(authenticationRequest.getPassword()));
            if(authenticationRequest.getAuthorities()!=null && authenticationRequest.getAuthorities().size()>0) {
                authenticationRequest.setAuthorities(authenticationRequest.getAuthorities());
            }else{
                authenticationRequest.getAuthorities().add(new Authority(AuthorityName.ROLE_USER));
            }
            UserDTO user = UserDTO.of(jwtUserDetailsService.createNewUser(authenticationRequest));
            return ResponseEntity.ok(user);
        }

    }

    @PostMapping("${jwt.route.authentication.refresh}")
    public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) {
        String authToken = request.getHeader(tokenHeader);
        final String token = authToken.substring(7);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        JwtUser user = (JwtUser) userDetailsService.loadUserByUsername(username);

        if (jwtTokenUtil.canTokenBeRefreshed(token, user.getLastPasswordResetDate())) {
            String refreshedToken = jwtTokenUtil.refreshToken(token);
            return ResponseEntity.ok(new JwtAuthenticationResponse(refreshedToken, getRoles(user)));
        } else {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @ExceptionHandler({AuthenticationException.class})
    public ResponseEntity<String> handleAuthenticationException(AuthenticationException e) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
    }

    /**
     * Authenticates the user. If something is wrong, an {@link AuthenticationException} will be thrown
     */
    private void authenticate(String username, String password) {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new AuthenticationException("User is disabled!", e);
        } catch (BadCredentialsException e) {
            throw new AuthenticationException("Bad credentials!", e);
        }
    }

    @RequestMapping(value = "user", method = RequestMethod.GET)
    public JwtUser getAuthenticatedUser(HttpServletRequest request) {
        String token = request.getHeader(tokenHeader).substring(7);
        System.out.println("Token value : " + token);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        JwtUser user = (JwtUser) userDetailsService.loadUserByUsername(username);
        return user;
    }
}
