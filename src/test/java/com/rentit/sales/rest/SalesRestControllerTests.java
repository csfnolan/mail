package com.rentit.sales.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rentit.RentitApplication;
import com.rentit.common.rest.dto.BusinessPeriodDTO;
import com.rentit.common.rest.dto.CustomerDTO;
import com.rentit.finance.domain.model.InvoiceStatus;
import com.rentit.finance.rest.dto.InvoiceDto;
import com.rentit.finance.rest.dto.RemittanceDto;
import com.rentit.inventory.rest.dto.DispatchDto;
import com.rentit.inventory.rest.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.domain.repository.PlantInventoryEntryRepository;
import com.rentit.sales.domain.model.POStatus;
import com.rentit.sales.rest.dto.POExtensionDTO;
import com.rentit.sales.rest.dto.PurchaseOrderDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.text.IsEmptyString.isEmptyOrNullString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RentitApplication.class) // Check if the name of this class is correct or not
@WebAppConfiguration
@DirtiesContext(classMode=DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Transactional
public class SalesRestControllerTests {
    @Autowired
    PlantInventoryEntryRepository repo;

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;

    @Autowired @Qualifier("_halObjectMapper")
    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }


    @Test
    public void testCreatePurchaseOrderReturnsHttpCreated() throws Exception {
        String jsonString = "{\"customer\":{\"name\":\"BuildIt\",\"contactPerson\":\"Chibaba\",\"email\":\"team2@gmail.com\",\"siteAddress\":\"Juhan Liivi 2\" },\"plant\":{\"_id\":\"1\"}, \"rentalPeriod\": {\"startDate\": \"2018-06-01\", \"endDate\": \"2018-06-05\"}}";

        mockMvc.perform(post("/api/sales/orders").content(jsonString).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    private PurchaseOrderDTO createPO() throws Exception {
        PurchaseOrderDTO order = new PurchaseOrderDTO();
        PlantInventoryEntryDTO plant = new PlantInventoryEntryDTO();
        CustomerDTO customer = new CustomerDTO();
        customer.setContactPerson("BuildIT");
        customer.setEmail("buildit@gmail.com");
        customer.setName("Fisayo");
        customer.setSiteAddress("Raatuse 22");
        plant.set_id(1L);
        order.setPlant(new Resource<>(plant));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.of(2018,6,27), LocalDate.of(2018,6,29)));
        order.setCustomer(customer);

        MvcResult result = mockMvc.perform(post("/api/sales/orders")
                .content(mapper.writeValueAsString(order))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        return mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderDTO.class);
    }

    private PurchaseOrderDTO dispatch(Long id) throws Exception {
        MvcResult result = mockMvc.perform(post("/api/sales/orders/"+id+"/dispatch")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        return mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderDTO.class);
    }

    private PurchaseOrderDTO delivery(Long id) throws Exception {
        MvcResult result = mockMvc.perform(post("/api/sales/orders/"+id+"/delivery")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        return mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderDTO.class);
    }

    private PurchaseOrderDTO rejectPlant(Long id) throws Exception {
        MvcResult result = mockMvc.perform(delete("/api/sales/orders/"+id+"/delivery")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        return mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderDTO.class);
    }

    private PurchaseOrderDTO returnPlant(Long id) throws Exception {
        MvcResult result = mockMvc.perform(delete("/api/sales/orders/"+id+"/dispatch")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        return mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderDTO.class);
    }

    @Test
    public void testGetAvailablePlants() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2018-06-27&endDate=2018-06-29"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();
        Resources<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Resources<PlantInventoryEntryDTO>>() {});
        assertThat(plants.getContent().size()).isEqualTo(3);

    }

    @Test
    public void testGetPlantById() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/inventory/plants/1"))
                .andExpect(status().isOk())
                .andReturn();
        Resource<PlantInventoryEntryDTO> plant = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Resource<PlantInventoryEntryDTO>>() {});

        assertThat(plant.getContent()).isNotEqualTo(null);
        assertThat(plant.getContent().getPrice()).isNotEqualTo(null);

    }


    @Test
    @WithMockUser(username="test",authorities={"ROLE_USER"})
    public void testCreatePurchaseOrder() throws Exception {
        PurchaseOrderDTO order = createPO();

        assertThat(order.get_id()).isNotEqualTo(null);
        assertThat(order.getStatus()).isEqualTo(POStatus.OPEN);
    }

    @Test
    @WithMockUser(username="test",authorities={"ROLE_DISPATCHER"})
    public void testGetPlantToBeDelivered() throws Exception {
        PurchaseOrderDTO order = createPO();


        MvcResult result = mockMvc.perform(get("/api/sales/orders/transfers?date="+order.getRentalPeriod().getStartDate()))
                .andExpect(status().isOk())
                .andReturn();
        List<Resource<DispatchDto>> plants = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<Resource<DispatchDto>>>() {});
        assertThat(plants.size()).isEqualTo(1);
    }

    @Test
    @WithMockUser(username="test",authorities={"ROLE_DISPATCHER"})
    public void testDispatchPlantInPO() throws Exception {
        PurchaseOrderDTO order = createPO();
        assertThat(order.get_id()).isNotEqualTo(null);
        assertThat(order.getStatus()).isEqualTo(POStatus.OPEN);
        order = dispatch(order.get_id());
        assertThat(order.getStatus()).isEqualTo(POStatus.PLANT_DISPATCHED);
    }

    @Test
    @WithMockUser(username="test",authorities={"ROLE_DISPATCHER"})
    public void testDeliverPlantInPO() throws Exception {
        PurchaseOrderDTO order = createPO();
        assertThat(order.get_id()).isNotEqualTo(null);
        assertThat(order.getStatus()).isEqualTo(POStatus.OPEN);
        order = dispatch(order.get_id());
        assertThat(order.getStatus()).isEqualTo(POStatus.PLANT_DISPATCHED);
        order = delivery(order.get_id());
        assertThat(order.getStatus()).isEqualTo(POStatus.PLANT_DELIVERED);


    }

    @Test
    @WithMockUser(username="test",authorities={"ROLE_DISPATCHER"})
    public void testRejectPlantInPO() throws Exception {
        PurchaseOrderDTO order = createPO();
        assertThat(order.get_id()).isNotEqualTo(null);
        assertThat(order.getStatus()).isEqualTo(POStatus.OPEN);
        order = dispatch(order.get_id());
        assertThat(order.getStatus()).isEqualTo(POStatus.PLANT_DISPATCHED);


        order = rejectPlant(order.get_id());
        assertThat(order.getStatus()==POStatus.PLANT_REJECTED_BY_CUSTOMER);
    }

    @Test
    @WithMockUser(username="test",authorities={"ROLE_DISPATCHER"})
    public void testReturnPlantInPO() throws Exception {
        PurchaseOrderDTO order = createPO();
        assertThat(order.get_id()).isNotEqualTo(null);
        assertThat(order.getStatus()).isEqualTo(POStatus.OPEN);
        order = dispatch(order.get_id());
        assertThat(order.getStatus()).isEqualTo(POStatus.PLANT_DISPATCHED);

        order = delivery(order.get_id());
        assertThat(order.getStatus()).isEqualTo(POStatus.PLANT_DELIVERED);

        order = returnPlant(order.get_id());
        assertThat(order.getStatus()).isEqualTo(POStatus.PLANT_RETURNED);
    }

    @Test
    @WithMockUser(username="test",authorities={"ROLE_USER"})
    public void testCancelPO() throws Exception {
        PurchaseOrderDTO order = createPO();
        assertThat(order.get_id()).isNotEqualTo(null);
        assertThat(order.getStatus().equals(POStatus.OPEN));
        MvcResult result = mockMvc.perform(delete("/api/sales/orders/"+order.get_id())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        Resource<PurchaseOrderDTO> po = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Resource<PurchaseOrderDTO>>() {});
        assertThat(po.getContent().get_id()).isEqualTo(order.get_id());
        assertThat(po.getContent().getStatus()).isEqualTo(POStatus.CANCELLED);
    }

    @Test
    @WithMockUser(username="test",authorities={"ROLE_ADMIN"})
    public void testExtensionRequest() throws Exception {
        PurchaseOrderDTO order = createPO();
        assertThat(order.get_id()).isNotEqualTo(null);
        assertThat(order.getStatus()).isEqualTo(POStatus.OPEN);
        order = dispatch(order.get_id());
        assertThat(order.getStatus()).isEqualTo(POStatus.PLANT_DISPATCHED);
        order = delivery(order.get_id());
        assertThat(order.getStatus()).isEqualTo(POStatus.PLANT_DELIVERED);

        POExtensionDTO extensionDTO = new POExtensionDTO();
        extensionDTO.setEndDate(LocalDate.of(2018,07,5));
        MvcResult result = mockMvc.perform(post("/api/sales/orders/"+order.get_id()+"/extensions")
                .content(mapper.writeValueAsString(extensionDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        Resource<PurchaseOrderDTO> po = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Resource<PurchaseOrderDTO>>() {});
        //System.out.println(po.getContent());
        assertThat(po.getContent().getStatus()).isEqualTo(POStatus.PLANT_DELIVERED);
        assertThat(po.getContent().getRentalPeriod().getEndDate()).isEqualTo(LocalDate.of(2018,07,5));
    }

    @Test
    @WithMockUser(username="test",authorities={"ROLE_SALES"})
    public void testFetchAllPos() throws Exception {
        createPO();
        MvcResult result = mockMvc.perform(get("/api/sales/orders?status=OPEN"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();
        Resources<PurchaseOrderDTO> orders = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Resources<PurchaseOrderDTO>>() {});
        assertThat(orders.getContent().size()).isEqualTo(1);

    }

   @Test
   @WithMockUser(username="test",authorities={"ROLE_SALES"})
    public void testGetPurchaseOrdersById() throws Exception {
       PurchaseOrderDTO order = createPO();
        MvcResult result = mockMvc.perform(get("/api/sales/orders/" + order.get_id()))
                .andExpect(status().isOk())
                .andReturn();

        Resource<PurchaseOrderDTO> po = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Resource<PurchaseOrderDTO>>() {});
        assertThat(po.getContent().get_id()).isEqualTo(order.get_id());

    }


    @Test
    @WithMockUser(username="test",authorities={"ROLE_FINANCE","ROLE_ADMIN"})
    public  void testSendReminders() throws Exception {
        PurchaseOrderDTO order = createPO();
        dispatch(order.get_id());
        delivery(order.get_id());
        returnPlant(order.get_id());
        InvoiceDto invoice = getInvoice(order.get_id(), InvoiceStatus.PENDING);
        acceptInvoice(invoice);
        mockMvc.perform(post("/api/finance/invoices/"+invoice.get_id()+"/reminders"))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    @WithMockUser(username="test",authorities={"ROLE_USER","ROLE_ADMIN"})
    public  void testSendRemittance() throws Exception {
        PurchaseOrderDTO order = createPO();
        dispatch(order.get_id());
        delivery(order.get_id());
        returnPlant(order.get_id());

        InvoiceDto invoice = getInvoice(order.get_id(), InvoiceStatus.PENDING);
        acceptInvoice(invoice);
        RemittanceDto remittanceDto = new RemittanceDto();
        remittanceDto.setAmount(invoice.getAmount());
        MvcResult result = mockMvc.perform(post("/api/finance/invoices/"+invoice.get_id()+"/remittances")
                .content(mapper.writeValueAsString(remittanceDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        Resource<InvoiceDto> invoiceResource = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Resource<InvoiceDto>>() {});
        assertThat(invoiceResource.getContent().getStatus()).isEqualTo(InvoiceStatus.PAID);
    }

    private InvoiceDto getInvoice(Long orderId, InvoiceStatus status) throws Exception {
        MvcResult result = mockMvc.perform(get("/api/finance/invoices?status="+status.name()))
                .andExpect(status().isOk())
                .andReturn();

        Resources<InvoiceDto> invoices = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Resources<InvoiceDto>>() {});
        assertThat(invoices.getContent().size()).isGreaterThan(0);
        InvoiceDto invoice = null;
        for (InvoiceDto i:invoices.getContent()) {
            if(i.getOrderId()==orderId){
                invoice = i;
            }
        }

        return invoice;
    }

    private InvoiceDto acceptInvoice(InvoiceDto invoice) throws Exception {
        invoice.setStatus(InvoiceStatus.OPEN);
        MvcResult result = mockMvc.perform(put("/api/finance/invoices/"+invoice.get_id())
                .content(mapper.writeValueAsString(invoice))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        Resource<InvoiceDto> invoiceResource = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Resource<InvoiceDto>>() {});

        return invoiceResource.getContent();
    }

}
