insert into plant_inventory_category (id, name) values (1, 'Excavator');
insert into plant_inventory_category (id, name) values (2, 'Dumper');
insert into plant_inventory_category (id, name) values (3, 'Dump Truck');
insert into plant_inventory_category (id, name) values (4, 'Loader');

insert into plant_inventory_entry (id, name, description, price,plant_inventory_category_id) values (1, 'Mini excavator', '1.5 Tonne Mini excavator', 150,1);
insert into plant_inventory_entry (id, name, description, price,plant_inventory_category_id) values (2, 'Mini excavator', '3 Tonne Mini excavator', 200,1);
insert into plant_inventory_entry (id, name, description, price,plant_inventory_category_id) values (3, 'Midi excavator', '5 Tonne Midi excavator', 250,1);
insert into plant_inventory_entry (id, name, description, price,plant_inventory_category_id) values (4, 'Midi excavator', '8 Tonne Midi excavator', 300,1);
insert into plant_inventory_entry (id, name, description, price,plant_inventory_category_id) values (5, 'Maxi excavator', '15 Tonne Large excavator', 400,1);
insert into plant_inventory_entry (id, name, description, price,plant_inventory_category_id) values (6, 'Maxi excavator', '20 Tonne Large excavator', 450,1);
insert into plant_inventory_entry (id, name, description, price,plant_inventory_category_id) values (7, 'HS dumper', '1.5 Tonne Hi-Swivel Dumper', 150,2);
insert into plant_inventory_entry (id, name, description, price,plant_inventory_category_id) values (8, 'FT dumper', '2 Tonne Front Tip Dumper', 180,2);
insert into plant_inventory_entry (id, name, description, price,plant_inventory_category_id) values (9, 'FT dumper', '2 Tonne Front Tip Dumper', 200,2);
insert into plant_inventory_entry (id, name, description, price,plant_inventory_category_id) values (10, 'FT dumper', '2 Tonne Front Tip Dumper', 300,2);
insert into plant_inventory_entry (id, name, description, price,plant_inventory_category_id) values (11, 'FT dumper', '3 Tonne Front Tip Dumper', 400,2);
insert into plant_inventory_entry (id, name, description, price,plant_inventory_category_id) values (12, 'Loader', 'Hewden Backhoe Loader', 200,4);
insert into plant_inventory_entry (id, name, description, price,plant_inventory_category_id) values (13, 'D-Truck', '15 Tonne Articulating Dump Truck', 250,3);
insert into plant_inventory_entry (id, name, description, price,plant_inventory_category_id) values (14, 'D-Truck', '30 Tonne Articulating Dump Truck', 300,3);

insert into plant_inventory_item (id, plant_info_id, serial_number) values (1, 1, 'A01');
insert into plant_inventory_item (id, plant_info_id, serial_number) values (2, 2, 'A02');
insert into plant_inventory_item (id, plant_info_id, serial_number) values (3, 3, 'A03');

INSERT INTO AUTHORITY (NAME) VALUES ('ROLE_USER');
INSERT INTO AUTHORITY (NAME) VALUES ('ROLE_ADMIN');
INSERT INTO AUTHORITY (NAME) VALUES ('ROLE_SALES');
INSERT INTO AUTHORITY (NAME) VALUES ('ROLE_DISPATCHER');
INSERT INTO AUTHORITY (NAME) VALUES ('ROLE_FINANCE');

-- INSERT INTO USER (ID, USERNAME, PASSWORD, FIRSTNAME, LASTNAME, EMAIL, ENABLED, LASTPASSWORDRESETDATE) VALUES (1, 'admin', '$2a$08$lDnHPz7eUkSi6ao14Twuau08mzhWrL4kyZGGU5xfiGALO/Vxd5DOi', 'admin', 'admin', 'admin@admin.com', 1, PARSEDATETIME('01-01-2016', 'dd-MM-yyyy'));
-- INSERT INTO USER (ID, USERNAME, PASSWORD, FIRSTNAME, LASTNAME, EMAIL, ENABLED, LASTPASSWORDRESETDATE) VALUES (2, 'user', '$2a$08$UkVvwpULis18S19S5pZFn.YHPZt3oaqHZnDwqbCW9pft6uFtkXKDC', 'user', 'user', 'enabled@user.com', 1, PARSEDATETIME('01-01-2016','dd-MM-yyyy'));
-- INSERT INTO USER (ID, USERNAME, PASSWORD, FIRSTNAME, LASTNAME, EMAIL, ENABLED, LASTPASSWORDRESETDATE) VALUES (3, 'disabled', '$2a$08$UkVvwpULis18S19S5pZFn.YHPZt3oaqHZnDwqbCW9pft6uFtkXKDC', 'user', 'user', 'disabled@user.com', 0, PARSEDATETIME('01-01-2016','dd-MM-yyyy'));
-- INSERT INTO USER (ID, USERNAME, PASSWORD, FIRSTNAME, LASTNAME, EMAIL, ENABLED, LASTPASSWORDRESETDATE) VALUES (4, 'sales', '$2a$08$UkVvwpULis18S19S5pZFn.YHPZt3oaqHZnDwqbCW9pft6uFtkXKDC', 'sales', 'user', 'sales@user.com', 1, PARSEDATETIME('01-01-2016','dd-MM-yyyy'));
-- INSERT INTO USER (ID, USERNAME, PASSWORD, FIRSTNAME, LASTNAME, EMAIL, ENABLED, LASTPASSWORDRESETDATE) VALUES (5, 'dispatch', '$2a$08$UkVvwpULis18S19S5pZFn.YHPZt3oaqHZnDwqbCW9pft6uFtkXKDC', 'dispatch', 'user', 'dispatch@user.com', 1, PARSEDATETIME('01-01-2016','dd-MM-yyyy'));
--
-- INSERT INTO USER_AUTHORITY (USER_ID, AUTHORITY_ID) VALUES (1, 1);
-- INSERT INTO USER_AUTHORITY (USER_ID, AUTHORITY_ID) VALUES (1, 2);
-- INSERT INTO USER_AUTHORITY (USER_ID, AUTHORITY_ID) VALUES (2, 1);
-- INSERT INTO USER_AUTHORITY (USER_ID, AUTHORITY_ID) VALUES (2, 2);
-- INSERT INTO USER_AUTHORITY (USER_ID, AUTHORITY_ID) VALUES (3, 1);
-- INSERT INTO USER_AUTHORITY (USER_ID, AUTHORITY_ID) VALUES (4, 1);
-- INSERT INTO USER_AUTHORITY (USER_ID, AUTHORITY_ID) VALUES (4, 3);
-- INSERT INTO USER_AUTHORITY (USER_ID, AUTHORITY_ID) VALUES (5, 1);
-- INSERT INTO USER_AUTHORITY (USER_ID, AUTHORITY_ID) VALUES (5, 4);
